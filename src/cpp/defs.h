
/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2008 - INRIA
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
 
#ifndef __DEFS_H__
#define __DEFS_H__

/* width of plain arc */
#define ARCW 1
/* width of hilited arc */
#define ARCH 3
/* width of node frontier */
#define NODEW 2
/* diameter of node */
#define NODEDIAM 20
/* default draw font size */
#define DRAWFONTSIZE 12

#endif /* __DEFS_H__ */

