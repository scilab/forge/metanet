// ====================================================================
// Allan CORNET - DIGITEO - 2012
// ====================================================================
function builder_cpp()
    files_src_cpp = [ ..
        "files.cpp", ..
        "loadg.cpp", ..
        "saveg.cpp"];

    src_cpp_path = get_absolute_file_path("builder_cpp.sce");
    CFLAGS = "-I" + src_cpp_path + " -I" + src_cpp_path + "../c/";

    tbx_build_src("metanet_cpp", files_src_cpp, "cpp", src_cpp_path, ..
                  [], "", CFLAGS);
endfunction
// ====================================================================
builder_cpp();
clear builder_cpp;
// ====================================================================
