// ====================================================================
// Allan CORNET - DIGITEO - 2012
// ====================================================================
function builder_c()
    files_src_c = [..
        "paths.c", ..
        "connex.c", ..
        "dmtree.c", ..
        "transc.c"];

    if getos() == "Windows" then
        files_src_c = [files_src_c, ..
            "DllmainMetanet.c"];
    end

    src_c_path = get_absolute_file_path("builder_c.sce");
    CFLAGS = "-I" + src_c_path;
    LIBS = [pathconvert(src_c_path +"../fortran/") + "libmetanet_f"; ..
            pathconvert(src_c_path +"../cpp/") + "libmetanet_cpp"];

    tbx_build_src("metanet_c", files_src_c, "c", ..
            src_c_path, LIBS, "", CFLAGS);
endfunction
// ====================================================================
builder_c();
clear builder_c;
// ====================================================================
