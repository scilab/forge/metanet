/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.edge;

import java.util.List;

import org.scilab.modules.metanet.utils.MetanetConstants;

import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxPoint;
import com.mxgraph.view.mxCellState;
import com.mxgraph.view.mxEdgeStyle.mxEdgeStyleFunction;

public class MetanetEdgeStyle implements mxEdgeStyleFunction {

    @Override
    public void apply(mxCellState state, mxCellState source, mxCellState target, List<mxPoint> points, List<mxPoint> result) {

        // if we're drag'n'droping an edge from a node to an other then
        // source or target can be null, then we just draw the basic straight
        // line
        if (source == null || target == null)
            return;
        double sourceDiam = ((mxCell) source.getCell()).getGeometry().getWidth();
        double targetDiam = ((mxCell) target.getCell()).getGeometry().getWidth();

        double referenceDiam = Math.min(sourceDiam, targetDiam);
        double ratio = referenceDiam / MetanetConstants.REFERENCE_NODE_DIAM;

        int indexOfCurrentEdge = 0;
        int numberOfEdgeOnSource = ((mxCell) source.getCell()).getEdgeCount(); // -1
                                                                               // as
                                                                               // the
                                                                               // current
        int j = 0;

        boolean previousEdgeSameDirection = true;

        for (int i = 0; i < numberOfEdgeOnSource; ++i) {
            mxCell tempEdge = (mxCell) ((mxCell) source.getCell()).getEdgeAt(i);

            // edge in the same direction
            if (tempEdge.getTarget().getId() == ((mxCell) target.getCell()).getId()) {

                if (((mxCell) state.getCell()).getId() == tempEdge.getId()) {
                    indexOfCurrentEdge = j;

                    break;
                }
                j++;
                previousEdgeSameDirection = true;
            }
            // edge in the opposite direction
            if (tempEdge.getSource().getId() == ((mxCell) target.getCell()).getId()) {
                j++;
                previousEdgeSameDirection = false;
            }

        }

        double x0 = source.getCenterX();
        double xe = target.getCenterX();
        double y0 = source.getCenterY();
        double ye = target.getCenterY();

        // TODO it's a hack, it happens only from .graph imported graph
        // where some node were at the same position

        if (x0 == xe && y0 == ye) {
            ye++;
        }

        double middleX = (x0 + xe) / 2;
        double middleY = (y0 + ye) / 2;
        double hypo = Math.sqrt((xe - x0) * (xe - x0) + (ye - y0) * (ye - y0));

        double angle = Math.acos(Math.abs(x0 - xe) / hypo);

        int factor = 0;

        if (indexOfCurrentEdge > 0) {
            factor = (int) Math.ceil(indexOfCurrentEdge / 2.0f);

            if (indexOfCurrentEdge % 2 == 1) {
                factor *= -1;
            }

        }

        // we need to invert the factor there's already an edge on the same
        // |factor|
        // and if this edge is on the opposite direction otherwise they will
        // superpose

        if (indexOfCurrentEdge % 2 == 0) {
            if (!previousEdgeSameDirection) {
                factor *= -1;
            }
        }

        // 0 right top
        // 1 left top
        // 2 left bottom
        // 3 right bottom
        if (x0 > xe) {
            if (y0 > ye) {
                // left top
                angle = Math.PI + angle;
            } else {
                // left bottom
                angle = Math.PI - angle;
            }
        } else {
            if (y0 < ye) {
                // right bottom
            } else {
                // right top

                angle = Math.PI * 2 - angle;
            }
        }

        double offSetHeight = factor * (MetanetConstants.EDGE_ARC_HEIGHT * state.getView().getScale() * ratio);

        double arcTopLeftX = (-hypo / 3) * Math.cos(angle) - (offSetHeight) * Math.sin(angle);
        double arcTopLeftY = (-hypo / 3) * Math.sin(angle) + (offSetHeight) * Math.cos(angle);

        double arcTopRightX = (hypo / 3) * Math.cos(angle) - (offSetHeight) * Math.sin(angle);
        double arcTopRightY = (hypo / 3) * Math.sin(angle) + (offSetHeight) * Math.cos(angle);
        /*
         * arcTopLeftX *= state.getView().getScale(); arcTopLeftY *=
         * state.getView().getScale();
         * 
         * arcTopRightX *= state.getView().getScale(); arcTopRightY *=
         * state.getView().getScale();
         */
        mxPoint arcTopRight = new mxPoint(middleX + arcTopRightX, middleY + arcTopRightY);
        mxPoint arcTopLeft = new mxPoint(middleX + arcTopLeftX, middleY + arcTopLeftY);
        result.add(arcTopLeft);
        result.add(arcTopRight);

    }

}
