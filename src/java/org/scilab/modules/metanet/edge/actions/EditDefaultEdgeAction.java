/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.edge.actions;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import org.scilab.modules.graph.ScilabGraph;
import org.scilab.modules.graph.actions.base.DefaultAction;
import org.scilab.modules.gui.bridge.colorchooser.SwingScilabColorChooser;
import org.scilab.modules.gui.menuitem.MenuItem;
import org.scilab.modules.metanet.edge.BasicEdge;
import org.scilab.modules.metanet.graph.MetanetDiagram;
import org.scilab.modules.metanet.utils.MetanetMessages;

import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxUtils;

public class EditDefaultEdgeAction extends DefaultAction {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    public static final String NAME = MetanetMessages.EDIT_DEFAULT_EDGE;
    public static final String SMALL_ICON = "";
    public static final int MNEMONIC_KEY = 0;
    public static final int ACCELERATOR_KEY = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();

    private JFrame mainFrame;
    private GridBagConstraints gbc;

    private static boolean windowAlreadyExist;
    private ArrayList<JTextField> changeFieldValueInputList;
    private ArrayList<String> fieldNamesList;
    private BasicEdge defaultEdge;

    private static final DecimalFormatSymbols FORMAT_SYMBOL = new DecimalFormatSymbols();
    private static final DecimalFormat CURRENT_FORMAT = new DecimalFormat("0.0####E00;0", FORMAT_SYMBOL);

    private static final InputVerifier VALIDATE_POSITIVE_DOUBLE = new InputVerifier() {
        @Override
        public boolean verify(javax.swing.JComponent arg0) {
            boolean ret = false;
            JFormattedTextField textField = (JFormattedTextField) arg0;
            try {
                BigDecimal value = new BigDecimal(textField.getText());
                if (value.compareTo(new BigDecimal(0)) >= 0) {
                    ret = true;
                }
            } catch (NumberFormatException e) {
                return ret;
            }
            return ret;

        };
    };

    static {
        FORMAT_SYMBOL.setDecimalSeparator('.');
        CURRENT_FORMAT.setDecimalFormatSymbols(FORMAT_SYMBOL);
        CURRENT_FORMAT.setParseIntegerOnly(false);
        CURRENT_FORMAT.setParseBigDecimal(true);
    }

    /**
     * Constructor
     * 
     * @param scilabGraph
     *            associated diagram
     */
    public EditDefaultEdgeAction(ScilabGraph scilabGraph) {
        super(scilabGraph);
    }

    /**
     * Menu for diagram menubar
     * 
     * @param scilabGraph
     *            associated diagram
     * @return the menu
     */
    public static MenuItem createMenu(ScilabGraph scilabGraph) {
        return createMenu(scilabGraph, EditDefaultEdgeAction.class);
    }

    /**
     * @param e
     *            parameter
     * @see org.scilab.modules.graph.actions.base.DefaultAction#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        MetanetDiagram diagram = (MetanetDiagram) getGraph(null);
        defaultEdge = diagram.getDefaultEdge();
        editDefaultNodeBox(e);
    }

    /**
	 * 
	 */
    private void editDefaultNodeBox(ActionEvent e) {

        /** Avoid to have this window created two times */
        if (windowAlreadyExist) {
            mainFrame.setVisible(true);
            return;
        }

        Icon scilabIcon = new ImageIcon(System.getenv("SCI") + "/modules/gui/images/icons/scilab.png");
        Image imageForIcon = ((ImageIcon) scilabIcon).getImage();

        mainFrame = new JFrame();
        windowAlreadyExist = true;

        mainFrame.setLayout(new GridBagLayout());
        mainFrame.setIconImage(imageForIcon);

        gbc = new GridBagConstraints();

        /* generate all the input from the data inside the default Node */
        String[] fieldsName = defaultEdge.getDataFieldsName();
        int numberOfFields = fieldsName.length;

        changeFieldValueInputList = new ArrayList<JTextField>(numberOfFields);
        fieldNamesList = new ArrayList<String>(numberOfFields);

        for (int i = 0; i < numberOfFields; i++) {
            JTextField changeFieldValueInput = new JTextField();

            addInputLine(i, fieldsName[i], changeFieldValueInput, defaultEdge.getDataFieldsValue(fieldsName[i]).toString());

            /**/
            fieldNamesList.add(fieldsName[i]);
            changeFieldValueInputList.add(changeFieldValueInput);
        }

        // change default edge color

        JButton changeStyleColorButton = new JButton(MetanetMessages.CHANGE_EDGE_COLOR);
        changeStyleColorButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                /* launch a color chooser window */
                SwingScilabColorChooser _colorChooser = new SwingScilabColorChooser(null);
                _colorChooser.displayAndWait();
                Color newColor = _colorChooser.getSelectedColor();

                if (newColor != null) {
                    getGraph(null).setCellStyles(mxConstants.STYLE_STROKECOLOR, mxUtils.hexString(newColor), new Object[] { defaultEdge });
                }
                /* update label color */

                mainFrame.setFocusable(true);
            }
        });

        gbc.gridy = numberOfFields + 1;
        gbc.gridx = 1;
        gbc.gridheight = 1;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 10, 0, 10);
        mainFrame.add(changeStyleColorButton, gbc);

        /* ok cancel and reset to default button */

        JButton okButton = new JButton(MetanetMessages.OK);
        JButton cancelButton = new JButton(MetanetMessages.CANCEL);

        gbc.gridx = 1;
        gbc.gridy = numberOfFields + 2;
        gbc.gridheight = gbc.gridwidth = 1;
        gbc.weightx = 1.;
        gbc.fill = GridBagConstraints.NONE;
        gbc.insets = new Insets(5, 0, 10, 5);
        mainFrame.add(okButton, gbc);

        gbc.gridx = 2;
        gbc.weightx = 0.;
        gbc.insets = new Insets(5, 0, 10, 10);
        mainFrame.add(cancelButton, gbc);

        // OK Listener
        okButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                for (int i = 0; i < changeFieldValueInputList.size(); ++i) {
                    Double value = Double.valueOf(changeFieldValueInputList.get(i).getText());
                    String fieldName = fieldNamesList.get(i);
                    defaultEdge.setDataFieldsValue(fieldName, value);

                }

                windowAlreadyExist = false;

                mainFrame.dispose();
            }

        });

        // Cancel Listener
        cancelButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                windowAlreadyExist = false;
                mainFrame.dispose();
            }
        });

        mainFrame.addWindowListener(new WindowListener() {
            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                windowAlreadyExist = false;
                mainFrame.dispose();
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowOpened(WindowEvent e) {
            }
        });

        mainFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        mainFrame.setTitle(MetanetMessages.EDIT_DEFAULT_NODE);
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(getGraph(e).getAsComponent());
        mainFrame.setVisible(true);
    }

    /**
	 * 
	 */
    private void addInputLine(int indexY, String labelName, JTextField inputField, double value) {
        addInputLine(indexY, labelName, inputField, String.valueOf(value));
        inputField = new JFormattedTextField(CURRENT_FORMAT);
        inputField.setInputVerifier(VALIDATE_POSITIVE_DOUBLE);
    }

    /**
	 * 
	 *
	 */

    private void addInputLine(int indexY, String labelName, JTextField inputField, String value) {
        gbc.gridy = indexY;

        /* create label */
        gbc.gridx = 0;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.NONE;
        gbc.insets = new Insets(0, 10, 0, 0);

        JLabel fieldNameLabel = new JLabel(labelName, SwingConstants.TRAILING);

        mainFrame.add(fieldNameLabel, gbc);

        /* create input field */
        gbc.gridx = 1;
        gbc.gridheight = 1;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 10, 0, 10);

        inputField.setText(value);

        // changeStyleColorButton.addActionListener(changeColorListener);

        mainFrame.add(inputField, gbc);
    }
}
