/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.edge.actions;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import org.scilab.modules.commons.gui.FindIconHelper;
import org.scilab.modules.graph.ScilabGraph;
import org.scilab.modules.graph.actions.base.DefaultAction;
import org.scilab.modules.gui.menuitem.MenuItem;
import org.scilab.modules.metanet.edge.BasicEdge;
import org.scilab.modules.metanet.graph.MetanetDiagram;
import org.scilab.modules.metanet.utils.MetanetMessages;

public class EdgeParametersAction extends DefaultAction {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    public static final String NAME = MetanetMessages.EDGE_PARAMETERS;
    public static final String SMALL_ICON = "";
    public static final int MNEMONIC_KEY = 0;
    public static final int ACCELERATOR_KEY = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();

    private static JFrame mainFrame;
    private static GridBagConstraints gbc;

    private static boolean windowAlreadyExist;
    private static ArrayList<JTextField> changeFieldValueInputList;
    private static ArrayList<String> fieldNamesList;
    private static JTextField nameInputField;

    private static final DecimalFormatSymbols FORMAT_SYMBOL = new DecimalFormatSymbols();
    private static final DecimalFormat CURRENT_FORMAT = new DecimalFormat("0.0####E00;0", FORMAT_SYMBOL);

    private static final InputVerifier VALIDATE_POSITIVE_DOUBLE = new InputVerifier() {
        @Override
        public boolean verify(javax.swing.JComponent arg0) {
            boolean ret = false;
            JFormattedTextField textField = (JFormattedTextField) arg0;
            try {
                BigDecimal value = new BigDecimal(textField.getText());
                if (value.compareTo(new BigDecimal(0)) >= 0) {
                    ret = true;
                }
            } catch (NumberFormatException e) {
                return ret;
            }
            return ret;

        };
    };

    static {
        FORMAT_SYMBOL.setDecimalSeparator('.');
        CURRENT_FORMAT.setDecimalFormatSymbols(FORMAT_SYMBOL);
        CURRENT_FORMAT.setParseIntegerOnly(false);
        CURRENT_FORMAT.setParseBigDecimal(true);
    }

    /**
     * Constructor
     * 
     * @param scilabGraph
     *            associated diagram
     */
    public EdgeParametersAction(ScilabGraph scilabGraph) {
        super(scilabGraph);
    }

    /**
     * Menu for diagram menubar
     * 
     * @param scilabGraph
     *            associated diagram
     * @return the menu
     */
    public static MenuItem createMenu(ScilabGraph scilabGraph) {
        return createMenu(scilabGraph, EdgeParametersAction.class);
    }

    /**
     * @param e
     *            parameter
     * @see org.scilab.modules.graph.actions.base.DefaultAction#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (((MetanetDiagram) getGraph(null)).getSelectionCell() != null) {
            MetanetDiagram diagram = (MetanetDiagram) getGraph(null);

            BasicEdge edge = ((BasicEdge) diagram.getSelectionCell());
            editEdgeBox(edge, diagram);
        }
    }

    public static void editEdgeBox(final BasicEdge edge, final MetanetDiagram graph) {

        /** Avoid to have this window created two times */
        if (windowAlreadyExist) {
            mainFrame.setVisible(true);
            return;
        }

        Icon scilabIcon = new ImageIcon(FindIconHelper.findIcon("scilab"));
        Image imageForIcon = ((ImageIcon) scilabIcon).getImage();

        mainFrame = new JFrame();
        windowAlreadyExist = true;

        mainFrame.setLayout(new GridBagLayout());
        mainFrame.setIconImage(imageForIcon);

        gbc = new GridBagConstraints();

        /* generate all the input from the data inside the default Node */
        String[] fieldsName = edge.getDataFieldsName();
        int numberOfFields = fieldsName.length;

        changeFieldValueInputList = new ArrayList<JTextField>(numberOfFields);
        fieldNamesList = new ArrayList<String>(numberOfFields);

        for (int i = 0; i < numberOfFields; i++) {
            JTextField changeFieldValueInput = new JTextField();

            addInputLine(i, fieldsName[i], changeFieldValueInput, edge.getDataFieldsValue(fieldsName[i]).toString());

            /**/
            fieldNamesList.add(fieldsName[i]);
            changeFieldValueInputList.add(changeFieldValueInput);
        }

        nameInputField = new JTextField();
        addInputLine(numberOfFields, MetanetMessages.NAME, nameInputField, edge.getEdgeName());

        /* ok cancel and reset to default button */

        JButton okButton = new JButton(MetanetMessages.OK);
        JButton cancelButton = new JButton(MetanetMessages.CANCEL);

        gbc.gridx = 1;
        gbc.gridy = numberOfFields + 1;
        gbc.gridheight = gbc.gridwidth = 1;
        gbc.weightx = 1.;
        gbc.fill = GridBagConstraints.NONE;
        gbc.insets = new Insets(5, 0, 10, 5);
        mainFrame.add(okButton, gbc);

        gbc.gridx = 2;
        gbc.weightx = 0.;
        gbc.insets = new Insets(5, 0, 10, 10);
        mainFrame.add(cancelButton, gbc);

        // OK Listener
        okButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                for (int i = 0; i < changeFieldValueInputList.size(); ++i) {
                    Double value = Double.valueOf(changeFieldValueInputList.get(i).getText());
                    String fieldName = fieldNamesList.get(i);
                    edge.setDataFieldsValue(fieldName, value);

                }

                edge.setEdgeName(nameInputField.getText());

                graph.getView().clear(edge, true, true);
                graph.refresh();

                windowAlreadyExist = false;
                mainFrame.dispose();
            }

        });

        // Cancel Listener
        cancelButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                windowAlreadyExist = false;
                mainFrame.dispose();
            }
        });

        mainFrame.addWindowListener(new WindowListener() {
            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                windowAlreadyExist = false;
                mainFrame.dispose();
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowOpened(WindowEvent e) {
            }
        });

        mainFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        mainFrame.setTitle(MetanetMessages.EDGE_PARAMETERS);
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(graph.getAsComponent());
        mainFrame.setVisible(true);
    }

    /**
	 * 
	 */
    private static void addInputLine(int indexY, String labelName, JTextField inputField, double value) {
        addInputLine(indexY, labelName, inputField, String.valueOf(value));
        inputField = new JFormattedTextField(CURRENT_FORMAT);
        inputField.setInputVerifier(VALIDATE_POSITIVE_DOUBLE);
    }

    /**
	 * 
	 *
	 */

    private static void addInputLine(int indexY, String labelName, JTextField inputField, String value) {
        gbc.gridy = indexY;

        /* create label */
        gbc.gridx = 0;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.NONE;
        gbc.insets = new Insets(0, 10, 0, 0);

        JLabel fieldNameLabel = new JLabel(labelName, SwingConstants.TRAILING);

        mainFrame.add(fieldNameLabel, gbc);

        /* create input field */
        gbc.gridx = 1;
        gbc.gridheight = 1;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 10, 0, 10);

        inputField.setText(value);

        // changeStyleColorButton.addActionListener(changeColorListener);

        mainFrame.add(inputField, gbc);
    }
}
