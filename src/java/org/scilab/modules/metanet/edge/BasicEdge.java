/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.edge;

import java.awt.MouseInfo;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map.Entry;

import org.scilab.modules.graph.ScilabGraph;
import org.scilab.modules.graph.actions.DeleteAction;
import org.scilab.modules.graph.utils.ScilabGraphConstants;
import org.scilab.modules.gui.bridge.contextmenu.SwingScilabContextMenu;
import org.scilab.modules.gui.contextmenu.ContextMenu;
import org.scilab.modules.gui.contextmenu.ScilabContextMenu;
import org.scilab.modules.gui.menuitem.MenuItem;
import org.scilab.modules.metanet.MetanetUIDObject;
import org.scilab.modules.metanet.edge.actions.ChangeEdgeColorAction;
import org.scilab.modules.metanet.edge.actions.EdgeParametersAction;

import com.mxgraph.model.mxGeometry;
import com.mxgraph.util.mxPoint;

public class BasicEdge extends MetanetUIDObject {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    private static final mxGeometry DEFAULT_GEOMETRY = new mxGeometry(0, 0, 80, 80);

    // FIXME: the ordering could be removed if the MetanetModel use a LinkedHashMap instead of an HashMap
    private int ordering;
    // FIXME: should be removed ; stored data should be stored into value as a list of Scilab value 
    private Hashtable<String, Object> data;
    // FIXME: The name have to be the toString of the 'value' returned by mxICell#getValue()
    //        the default component will use toString() to render the value of a cell
    private String edgeName;
    // FIXME: should be removed as this is a style property, the accessors have to update the style directly
    private double strokeWidth;

    public BasicEdge() {
        super();
        setVertex(false);
        setEdge(true);
        edgeName = "";
        data = new Hashtable<String, Object>();
        setGeometry(DEFAULT_GEOMETRY);
    }

    public BasicEdge(boolean directed) {
        super();
        setVertex(false);
        setEdge(true);
        edgeName = "";
        if (directed) {
            setStyle("directedEdge");
        }
        data = new Hashtable<String, Object>();
        setGeometry(DEFAULT_GEOMETRY);
    }

    public BasicEdge(BasicEdge defaultEdge) {
        super();
        setVertex(false);
        setEdge(true);
        edgeName = "";
        data = (Hashtable<String, Object>) defaultEdge.getData().clone();
        geometry = defaultEdge.getGeometry();
        style = defaultEdge.getStyle();
        strokeWidth = defaultEdge.getStrokeWidth();
        if (getGeometry() == null) {
            setGeometry(DEFAULT_GEOMETRY);
        }
    }

    /**
     * @param ordering
     *            order value
     */
    public void setOrdering(int ordering) {
        this.ordering = ordering;
    }

    /**
     * @return order value
     */
    public int getOrdering() {
        return ordering;
    }

    /**
     * Add a point at the position
     * 
     * @param x
     *            X coordinate
     * @param y
     *            Y coordinate
     */
    public void addPoint(double x, double y) {
        mxPoint point = new mxPoint(x, y);

        if (getGeometry().getPoints() == null) {
            getGeometry().setPoints(new ArrayList<mxPoint>());
        }
        getGeometry().getPoints().add(point);
    }

    /**
	 * 
	 */
    public void addDataField(String fieldName, Object value) {
        data.put(fieldName, value);
    }

    /**
     * 
     * @return
     */
    public String[] getDataFieldsName() {
        return data.keySet().toArray(new String[0]);
    }

    /**
     * 
     * @return
     */
    public Object getDataFieldsValue(String FieldName) {
        return data.get(FieldName);
    }

    /**
     * 
     * @return
     */

    public Hashtable<String, Object> getData() {
        return data;
    }

    /**
     * 
     * @return
     */

    public void setData(Hashtable<String, Object> data) {
        this.data = data;
    }

    /**
     * 
     * @return
     */
    public Object setDataFieldsValue(String fieldName, Object value) {
        return data.put(fieldName, value);
    }

    /**
     * @param strokeWidth
     *            width of the edge's stroke
     */

    public void setStrokeWidth(double strokeWidth) {
        this.strokeWidth = strokeWidth;
    }

    /**
     * @return strokeWidth width of the edge's stroke
     */
    public double getStrokeWidth() {
        return strokeWidth;
    }

    /**
     * @param edgeName
     *            name of the edge value
     */

    public void setEdgeName(String edgeName) {
        this.edgeName = edgeName;
    }

    /**
     * @return nodeName name of the node
     */
    public String getEdgeName() {
        return edgeName;
    }

    /**
     * Open the contextual menu of the link
     * 
     * @param graph
     *            The associated graph
     */
    public void openContextMenu(ScilabGraph graph) {
        ContextMenu menu = ScilabContextMenu.createContextMenu();

        MenuItem value = EdgeParametersAction.createMenu(graph);
        // menuList.put(EdgeParametersAction.class, value);
        menu.add(value);
        /*--- */
        menu.getAsSimpleContextMenu().addSeparator();
        /*--- */

        menu.add(DeleteAction.createMenu(graph));
        /*--- */

        menu.add(ChangeEdgeColorAction.createMenu(graph));

        ((SwingScilabContextMenu) menu.getAsSimpleContextMenu()).setLocation(MouseInfo.getPointerInfo().getLocation().x, MouseInfo.getPointerInfo()
                .getLocation().y);

        menu.setVisible(true);
    }

    public String getToolTipText() {
        StringBuffer result = new StringBuffer();
        result.append(ScilabGraphConstants.HTML_BEGIN);

        result.append("Edge Name : " + getEdgeName() + ScilabGraphConstants.HTML_NEWLINE);

        for (Entry<String, Object> entry : data.entrySet()) {
            String fieldName = entry.getKey();
            Object value = entry.getValue();
            result.append(fieldName + " : " + value + ScilabGraphConstants.HTML_NEWLINE);
        }
        result.append("id : " + getId() + ScilabGraphConstants.HTML_NEWLINE);
        result.append(ScilabGraphConstants.HTML_END);
        return result.toString();
    }
}
