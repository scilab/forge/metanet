/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - DIGITEO - Allan SIMON
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.node;

import java.util.Hashtable;

public class DefaultNode extends BasicNode {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    private static final int DEFAULT_DIAMETER = 30;

    private Boolean nameDisplayedInLabel;

    private Hashtable<String, Boolean> dataInLabel;

    public DefaultNode() {
        super();
        nameDisplayedInLabel = false;
        dataInLabel = new Hashtable<String, Boolean>();
        setDiameter(DEFAULT_DIAMETER);
    }

    /**
	 * 
	 */
    @Override
    public void addDataField(String fieldName, Object value) {
        this.getData().put(fieldName, value);
        dataInLabel.put(fieldName, false);
    }

    /**
	 * 
	 */
    public void addDataInLabel(String fieldName, Boolean value) {
        dataInLabel.put(fieldName, value);
    }

    /**
     * 
     * @return
     */
    public Boolean isDataInLabel(String fieldName) {
        return dataInLabel.get(fieldName);
    }

    /**
     * 
     * @return
     */
    public Object setDataInLabelValue(String fieldName, Boolean value) {
        return dataInLabel.put(fieldName, value);
    }

    /**
     * 
     * @return
     */
    @Override
    public void removeDataField(String fieldName) {
        dataInLabel.remove(fieldName);
    }

    /**
     * 
     * @return
     */

    public Hashtable<String, Boolean> getDataInLabel() {
        return dataInLabel;
    }

    /**
     * 
     * @return
     */

    public void setDataInLabel(Hashtable<String, Boolean> dataInLabel) {
        this.dataInLabel = dataInLabel;
    }

    /**
	 * 
	 */

    public Boolean isNameDiplayedInLabel() {
        return this.nameDisplayedInLabel;
    }

    /**
	 * 
	 */

    public void setNameDiplayedInLabel(Boolean nameDisplayedInLabel) {
        this.nameDisplayedInLabel = nameDisplayedInLabel;
    }

}
