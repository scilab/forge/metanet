/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.node.actions;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Map.Entry;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import org.scilab.modules.graph.ScilabGraph;
import org.scilab.modules.graph.actions.base.DefaultAction;
import org.scilab.modules.graph.utils.ScilabGraphConstants;
import org.scilab.modules.gui.bridge.colorchooser.SwingScilabColorChooser;
import org.scilab.modules.gui.menuitem.MenuItem;
import org.scilab.modules.metanet.graph.MetanetDiagram;
import org.scilab.modules.metanet.node.BasicNode;
import org.scilab.modules.metanet.node.DefaultNode;
import org.scilab.modules.metanet.utils.MetanetMessages;

import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxUtils;

public class EditDefaultNodeAction extends DefaultAction {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    public static final String NAME = MetanetMessages.EDIT_DEFAULT_NODE;
    public static final String SMALL_ICON = "";
    public static final int MNEMONIC_KEY = 0;
    public static final int ACCELERATOR_KEY = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();

    private JFrame mainFrame;
    private GridBagConstraints gbc;

    private static boolean windowAlreadyExist;
    private ArrayList<JTextField> changeFieldValueInputList;
    private ArrayList<JCheckBox> inLabelCheckBoxList;
    private ArrayList<String> fieldNamesList;
    private DefaultNode defaultNode;
    private JTextField diameterInputField;
    private JCheckBox diameterInNodeLabelCheckBox;
    private JCheckBox nodeNameInNodeLabelCheckBox;

    private static final DecimalFormatSymbols FORMAT_SYMBOL = new DecimalFormatSymbols();
    private static final DecimalFormat CURRENT_FORMAT = new DecimalFormat("0.0####E00;0", FORMAT_SYMBOL);

    private static final InputVerifier VALIDATE_POSITIVE_DOUBLE = new InputVerifier() {
        @Override
        public boolean verify(javax.swing.JComponent arg0) {
            boolean ret = false;
            JFormattedTextField textField = (JFormattedTextField) arg0;
            try {
                BigDecimal value = new BigDecimal(textField.getText());
                if (value.compareTo(new BigDecimal(0)) >= 0) {
                    ret = true;
                }
            } catch (NumberFormatException e) {
                return ret;
            }
            return ret;

        };
    };

    static {
        FORMAT_SYMBOL.setDecimalSeparator('.');
        CURRENT_FORMAT.setDecimalFormatSymbols(FORMAT_SYMBOL);
        CURRENT_FORMAT.setParseIntegerOnly(false);
        CURRENT_FORMAT.setParseBigDecimal(true);
    }

    /**
     * Constructor
     * 
     * @param scilabGraph
     *            associated diagram
     */
    public EditDefaultNodeAction(ScilabGraph scilabGraph) {
        super(scilabGraph);
    }

    /**
     * Menu for diagram menubar
     * 
     * @param scilabGraph
     *            associated diagram
     * @return the menu
     */
    public static MenuItem createMenu(ScilabGraph scilabGraph) {
        return createMenu(scilabGraph, EditDefaultNodeAction.class);
    }

    /**
     * @param e
     *            parameter
     * @see org.scilab.modules.graph.actions.base.DefaultAction#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        MetanetDiagram diagram = (MetanetDiagram) getGraph(null);
        defaultNode = diagram.getDefaultNode();
        editDefaultNodeBox(e);
    }

    /**
	 * 
	 */
    private void editDefaultNodeBox(ActionEvent e) {

        /** Avoid to have this window created two times */
        if (windowAlreadyExist) {
            mainFrame.setVisible(true);
            return;
        }

        Icon scilabIcon = new ImageIcon(System.getenv("SCI") + "/modules/gui/images/icons/scilab.png");
        Image imageForIcon = ((ImageIcon) scilabIcon).getImage();

        mainFrame = new JFrame();
        windowAlreadyExist = true;

        mainFrame.setLayout(new GridBagLayout());
        mainFrame.setIconImage(imageForIcon);

        gbc = new GridBagConstraints();

        /* generate all the input from the data inside the default Node */
        String[] fieldsName = defaultNode.getDataFieldsName();
        int numberOfFields = fieldsName.length;

        changeFieldValueInputList = new ArrayList<JTextField>(numberOfFields);
        fieldNamesList = new ArrayList<String>(numberOfFields);
        inLabelCheckBoxList = new ArrayList<JCheckBox>(numberOfFields);

        for (int i = 0; i < numberOfFields; i++) {
            String currentFieldValue = defaultNode.getDataFieldsValue(fieldsName[i]).toString();
            boolean isCurrentFieldInLabel = defaultNode.isDataInLabel(fieldsName[i]);
            JTextField changeFieldValueInput = new JTextField(currentFieldValue);

            JCheckBox inNodeLabel = new JCheckBox();
            inNodeLabel.setSelected(isCurrentFieldInLabel);

            addInputLine(i, inNodeLabel, fieldsName[i], changeFieldValueInput, currentFieldValue);

            /**/
            fieldNamesList.add(fieldsName[i]);
            inLabelCheckBoxList.add(inNodeLabel);
            changeFieldValueInputList.add(changeFieldValueInput);
        }

        diameterInputField = new JTextField();
        diameterInNodeLabelCheckBox = new JCheckBox();
        numberOfFields++;
        addInputLine(numberOfFields, diameterInNodeLabelCheckBox, MetanetMessages.DIAMETER, diameterInputField, defaultNode.getDiameter());

        nodeNameInNodeLabelCheckBox = new JCheckBox();
        nodeNameInNodeLabelCheckBox.setSelected(defaultNode.isNameDiplayedInLabel());
        numberOfFields++;
        addInputLine(numberOfFields, nodeNameInNodeLabelCheckBox, MetanetMessages.NAME, null, null);
        // change default node color

        JButton changeStyleColorButton = new JButton(MetanetMessages.BORDER_COLOR);
        changeStyleColorButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                /* launch a color chooser window */
                SwingScilabColorChooser _colorChooser = new SwingScilabColorChooser(null);
                _colorChooser.displayAndWait();
                Color newColor = _colorChooser.getSelectedColor();

                if (newColor != null) {
                    getGraph(null).setCellStyles(mxConstants.STYLE_STROKECOLOR, mxUtils.hexString(newColor), new Object[] { defaultNode });
                }
                /* update label color */

                mainFrame.setFocusable(true);
            }
        });
        numberOfFields++;
        gbc.gridy = numberOfFields;
        gbc.gridx = 1;
        gbc.gridheight = 1;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 10, 0, 10);
        mainFrame.add(changeStyleColorButton, gbc);

        /* ok cancel and reset to default button */

        JButton okButton = new JButton(MetanetMessages.OK);
        JButton cancelButton = new JButton(MetanetMessages.CANCEL);

        numberOfFields++;
        gbc.gridx = 1;
        gbc.gridy = numberOfFields;
        gbc.gridheight = gbc.gridwidth = 1;
        gbc.weightx = 1.;
        gbc.fill = GridBagConstraints.NONE;
        gbc.insets = new Insets(5, 0, 10, 5);
        mainFrame.add(okButton, gbc);

        gbc.gridx = 2;
        gbc.weightx = 0.;
        gbc.insets = new Insets(5, 0, 10, 10);
        mainFrame.add(cancelButton, gbc);

        // OK Listener
        okButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                for (int i = 0; i < changeFieldValueInputList.size(); ++i) {
                    Double value = Double.valueOf(changeFieldValueInputList.get(i).getText());
                    Boolean isDisplayInLabel = inLabelCheckBoxList.get(i).isSelected();
                    String fieldName = fieldNamesList.get(i);
                    defaultNode.setDataFieldsValue(fieldName, value);
                    defaultNode.setDataInLabelValue(fieldName, isDisplayInLabel);

                }
                defaultNode.setDiameter(Double.valueOf(diameterInputField.getText()));
                defaultNode.setNameDiplayedInLabel(nodeNameInNodeLabelCheckBox.isSelected());

                MetanetDiagram diagram = (MetanetDiagram) getGraph(null);
                int nbObjs = diagram.getModel().getChildCount(diagram.getDefaultParent());
                // update already existing nodes
                for (int i = 0; i < nbObjs; i++) {
                    Object currentObject = diagram.getModel().getChildAt(diagram.getDefaultParent(), i);
                    if (currentObject instanceof BasicNode) {
                        BasicNode currentNode = (BasicNode) currentObject;

                        StringBuffer result = new StringBuffer();
                        result.append(ScilabGraphConstants.HTML_BEGIN);

                        if (defaultNode.isNameDiplayedInLabel()) {
                            result.append(currentNode.getNodeName() + ScilabGraphConstants.HTML_NEWLINE);
                        }
                        // other fields
                        for (Entry<String, Boolean> entry : defaultNode.getDataInLabel().entrySet()) {

                            if (entry.getValue()) {

                                result.append(currentNode.getDataFieldsValue(entry.getKey()) + ScilabGraphConstants.HTML_NEWLINE);

                            }
                        }

                        result.append(ScilabGraphConstants.HTML_END);
                        ((BasicNode) currentObject).setValue(result.toString());
                    }
                }

                windowAlreadyExist = false;

                mainFrame.dispose();
            }

        });

        // Cancel Listener
        cancelButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                windowAlreadyExist = false;
                mainFrame.dispose();
            }
        });

        mainFrame.addWindowListener(new WindowListener() {
            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                windowAlreadyExist = false;
                mainFrame.dispose();
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowOpened(WindowEvent e) {
            }
        });

        mainFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        mainFrame.setTitle(MetanetMessages.EDIT_DEFAULT_NODE);
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(getGraph(e).getAsComponent());
        mainFrame.setVisible(true);
    }

    /**
	 * 
	 */
    private void addInputLine(int indexY, JCheckBox inNodeLabel, String labelName, JTextField inputField, double value) {
        addInputLine(indexY, inNodeLabel, labelName, inputField, String.valueOf(value));
        inputField = new JFormattedTextField(CURRENT_FORMAT);
        inputField.setInputVerifier(VALIDATE_POSITIVE_DOUBLE);
    }

    /**
	 * 
	 *
	 */

    private void addInputLine(int indexY, JCheckBox inNodeLabel, String labelName, JTextField inputField, String value) {
        gbc.gridy = indexY;

        /* create label */
        gbc.gridx = 0;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.NONE;
        gbc.insets = new Insets(0, 10, 0, 0);

        mainFrame.add(inNodeLabel, gbc);

        /* create label */
        gbc.gridx = 1;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.NONE;
        gbc.insets = new Insets(0, 10, 0, 0);

        JLabel fieldNameLabel = new JLabel(labelName, SwingConstants.TRAILING);

        mainFrame.add(fieldNameLabel, gbc);

        /* create input field */

        if (inputField != null) {
            gbc.gridx = 2;
            gbc.gridheight = 1;
            gbc.gridwidth = GridBagConstraints.REMAINDER;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.insets = new Insets(5, 10, 0, 10);

            inputField.setText(value);

            mainFrame.add(inputField, gbc);
        }

        // changeStyleColorButton.addActionListener(changeColorListener);

    }
}
