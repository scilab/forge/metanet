/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.io;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.scilab.modules.graphic_objects.figure.ColorMap;
import org.scilab.modules.graphic_objects.figure.Figure;
import org.scilab.modules.graphic_objects.graphicModel.GraphicModel;
import org.scilab.modules.javasci.JavasciException;
import org.scilab.modules.javasci.Scilab;
import org.scilab.modules.metanet.edge.BasicEdge;
import org.scilab.modules.metanet.node.BasicNode;
import org.scilab.modules.metanet.node.DefaultNode;
import org.scilab.modules.types.ScilabDouble;
import org.scilab.modules.types.ScilabMList;
import org.scilab.modules.types.ScilabString;
import org.scilab.modules.types.ScilabTList;
import org.scilab.modules.types.ScilabType;

import com.mxgraph.model.mxGeometry;

public class GraphReader {

    private static DefaultNode defaultNode;
    private static BasicEdge defaultEdge;

    private static boolean directed;

    private static ColorMap scilab_to_rgbColor;

    public static HashMap<String, Object> convertTListToDiagram(ScilabTList data) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        HashMap<Integer, BasicNode> indexedNodes = new HashMap<Integer, BasicNode>();

        defaultEdge = new BasicEdge();
        defaultNode = new DefaultNode();

        List<BasicNode> nodes = new ArrayList<BasicNode>();
        List<BasicEdge> edges = new ArrayList<BasicEdge>();

        ScilabTList graphData = data;

        // will be used to change coordinate
        double minX = Double.MAX_VALUE;
        double minY = Double.MAX_VALUE;

        try {
            isAValidGraphStructure(data);
        } catch (WrongTypeException e2) {
            e2.printStackTrace();
            return null;
        } catch (WrongStructureException e) {
            e.printStackTrace();
            return null;
        }

        // if the graph is directed or not
        if (((ScilabDouble) graphData.get(3)).getRealPart()[0][0] == 1.0) {
            directed = true;
        } else {
            directed = false;
        }

        // read all nodes

        ScilabMList nodesMList = ((ScilabMList) graphData.get(4));
        int numberOfNodes = getNbNodes(nodesMList);

        fillDefaultNode(nodesMList);

        for (int i = 0; i < numberOfNodes; ++i) {
            try {
                BasicNode currentNode = fillNodeStructure(i, nodesMList);
                currentNode.setOrdering(i);
                indexedNodes.put(i + 1, currentNode);

                nodes.add(currentNode);
                minX = Math.min(minX, currentNode.getGeometry().getX());
                minY = Math.min(minY, currentNode.getGeometry().getY());

            } catch (GraphReaderException e) {
                e.printStackTrace();
                return null;
            }
        }

        // read all edges

        ScilabMList edgesMList = ((ScilabMList) graphData.get(5));
        int numberOfEdges = getNbEdges(edgesMList);

        fillDefaultEdge(edgesMList);

        for (int i = 0; i < numberOfEdges; ++i) {
            try {
                BasicEdge currentEdge = fillEdgeStructure(i, edgesMList, nodes);
                currentEdge.setOrdering(i);
                edges.add(currentEdge);
            } catch (GraphReaderException e) {
                e.printStackTrace();
                return null;
            }
        }

        // we update coord as for scilab (0,0) is bottom left
        // + 20 is to have some space between the lowest node and the graph
        // border
        double offsetX = -minX + 20;
        double offsetY = -minY + 20;

        for (int i = 0; i < nodes.size(); ++i) {
            nodes.get(i).getGeometry().setX(nodes.get(i).getGeometry().getX() + offsetX);
            nodes.get(i).getGeometry().setY(nodes.get(i).getGeometry().getY() + offsetY);
        }

        // Read diagrams properties
        HashMap<String, Object> properties = null;

        properties = fillDiagrammProperties(data);

        // put all data
        result.put("Nodes", nodes);
        result.put("Edges", edges);
        result.put("Directed", directed);
        result.put("DefaultNode", defaultNode);
        result.put("DefaultEdge", defaultEdge);
        result.put("Properties", properties);

        return result;
    }

    /**
     * @param name
     *            hdf5 file
     * @return diagram structure
     */
    public static HashMap<String, Object> readDiagramFromScilab(String name) {
        generateScilab2RGBColor();
        ScilabTList data;
        try {
            data = (ScilabTList) Scilab.getInCurrentScilabSession(name);
            return convertTListToDiagram(data);
        } catch (JavasciException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 
     * @param data
     * @throws WrongTypeException
     * @throws WrongStructureException
     */

    private static void isAValidGraphStructure(ScilabTList data) throws WrongTypeException, WrongStructureException {

        int numberOfFieldInEGdata = 6;
        String[] realNameOfGraphFields = new String[] { "graph", "version", "name", "directed", "nodes", "edges" };

        // we test if the structure as enough field
        if (data.size() < numberOfFieldInEGdata) {
            throw new WrongStructureException();
        }

        // the first field is a list of string containing the name of the other
        // fields
        if (!(data.get(0) instanceof ScilabString)) {
            throw new WrongTypeException();
        }

        String[] nameOfGraphFields = getNameOfFieldsInStructure(data);
        // check if all the expecting field's name are present
        if (nameOfGraphFields.length < numberOfFieldInEGdata) {
            throw new WrongStructureException();
        }
        for (int i = 0; i < numberOfFieldInEGdata; i++) {
            if (!nameOfGraphFields[i].equals(realNameOfGraphFields[i])) {
                throw new WrongStructureException();
            }
        }

        // the second field must contain the version used to create the graph
        if (!(data.get(1) instanceof ScilabString)) {
            throw new WrongTypeException();
        }

        // the third field is the name of the diagram
        if (!(data.get(2) instanceof ScilabString)) {
            throw new WrongTypeException();
        }

        // the 4th field indicate if the graph is directed or not
        if (!(data.get(3) instanceof ScilabDouble)) {
            throw new WrongTypeException();
        }

        // the 5th field must contain a MList of nodes structure
        if (!(data.get(4) instanceof ScilabMList)) {
            throw new WrongTypeException();
        }

        // the 5th field must contain a MList of edges structure
        if (!(data.get(5) instanceof ScilabMList)) {
            throw new WrongTypeException();
        }

    }

    /**
     * 
     * @param structure
     * @return
     */

    public static String[] getNameOfFieldsInStructure(ScilabTList structure) {
        return ((ScilabString) structure.get(0)).getData()[0];
    }

    /**
     * 
     * @param data
     * @return
     */

    public static int getNbNodes(ScilabMList nodes) {
        // there's a field with contain already the number of nodes
        double numberOfNodes = ((ScilabDouble) nodes.get(1)).getRealPart()[0][0];
        return (int) numberOfNodes;
    }

    /**
     * 
     * @param data
     * @return
     */

    public static int getNbEdges(ScilabMList edges) {
        // there's no field containing the number of edges
        int numberOfEdges = ((ScilabDouble) edges.get(1)).getWidth();
        return numberOfEdges;
    }

    /**
     * 
     * @param nodeNumber
     * @param nodes
     * @return
     * @throws WrongStructureException
     * @throws WrongTypeException
     */

    private static BasicNode fillNodeStructure(int nodeNumber, ScilabMList nodes) throws WrongStructureException, WrongTypeException {

        String colorStyle = "";

        ScilabMList graphics = (ScilabMList) nodes.get(2);

        BasicNode tempNode = new BasicNode();
        ScilabMList nodeData = (ScilabMList) nodes.get(3);
        int numberOfDataField = nodeData.get(0).getWidth() - 1;

        // as the field 0 in a scilabMList is the name of the list
        for (int i = 1; i <= numberOfDataField; i++) {

            String fieldName = ((ScilabString) nodeData.get(0)).getData()[0][i];

            if (nodeData.get(i) instanceof ScilabDouble) {
                double value = ((ScilabDouble) nodeData.get(i)).getRealPart()[0][nodeNumber];
                tempNode.addDataField(fieldName, value);
            }

        }

        int currentFieldNumber = 3;
        String nodeName = ((ScilabString) graphics.get(currentFieldNumber)).getData()[0][nodeNumber];
        currentFieldNumber++;

        if (graphics.getWidth() == 13) {
            if (graphics.get(currentFieldNumber) instanceof ScilabString) {
                final String nodeId = ((ScilabString) graphics.get(currentFieldNumber)).getData()[0][nodeNumber];
                if (nodeId != null && !nodeId.isEmpty()) {
                    tempNode.setId(nodeId);
                }
            }
            currentFieldNumber++;
        }

        double xPosition = ((ScilabDouble) graphics.get(currentFieldNumber)).getRealPart()[0][nodeNumber];
        currentFieldNumber++;

        double yPosition = ((ScilabDouble) graphics.get(currentFieldNumber)).getRealPart()[0][nodeNumber];
        currentFieldNumber++;

        int type = (int) ((ScilabDouble) graphics.get(currentFieldNumber)).getRealPart()[0][nodeNumber];
        currentFieldNumber++;

        double diam = ((ScilabDouble) graphics.get(currentFieldNumber)).getRealPart()[0][nodeNumber];
        currentFieldNumber++;

        double border = ((ScilabDouble) graphics.get(currentFieldNumber)).getRealPart()[0][nodeNumber];
        currentFieldNumber++;

        // node border color
        final Color currentNodeColor;
        // node background color
        final Color backgroundColor;
        // new structure, store as RGB
        if (((ScilabDouble) graphics.get(currentFieldNumber)).getHeight() == 3) {
            double[][] rgbColor = ((ScilabDouble) graphics.get(currentFieldNumber)).getRealPart();
            currentNodeColor = new Color((int) rgbColor[0][nodeNumber], (int) rgbColor[1][nodeNumber], (int) rgbColor[2][nodeNumber]);
            backgroundColor = Color.WHITE;
        } else if (((ScilabDouble) graphics.get(currentFieldNumber)).getHeight() == 2) {
            // old structure store as the scilab color number
            double[][] scilabColor = ((ScilabDouble) graphics.get(currentFieldNumber)).getRealPart();
            if ((int) scilabColor[0][nodeNumber] != 0) {
                float[] rgb = scilab_to_rgbColor.getScilabColor((int) (scilabColor[0][nodeNumber] - 1));
                currentNodeColor = new Color(rgb[0], rgb[1], rgb[2]);
            } else {
                currentNodeColor = Color.BLACK;
            }

            // old structure store as the scilab color number
            scilabColor = ((ScilabDouble) graphics.get(currentFieldNumber)).getRealPart();
            if ((int) scilabColor[1][nodeNumber] != 0) {
                float[] rgb = scilab_to_rgbColor.getScilabColor((int) (scilabColor[1][nodeNumber] - 1));
                backgroundColor = new Color(rgb[0], rgb[1], rgb[2]);
            } else {
                backgroundColor = Color.WHITE;
            }
        } else {
            currentNodeColor = Color.BLACK;
            backgroundColor = Color.WHITE;
        }
        colorStyle = ";strokeColor=#" + Integer.toHexString(currentNodeColor.getRGB()).substring(2);
        colorStyle += ";fillColor=#" + Integer.toHexString(backgroundColor.getRGB()).substring(2);

        // if ()
        // double xPosition = ((ScilabDouble)
        // graphics.get(4)).getRealPart()[0][nodeNumber];
        // double yPosition = ((ScilabDouble)
        // graphics.get(5)).getRealPart()[0][nodeNumber];
        // int type = (int) ((ScilabDouble)
        // graphics.get(6)).getRealPart()[0][nodeNumber];
        // double diam = ((ScilabDouble)
        // graphics.get(7)).getRealPart()[0][nodeNumber];
        // double border = ((ScilabDouble)
        // graphics.get(8)).getRealPart()[0][nodeNumber];

        if (diam == 0) {
            diam = defaultNode.getDiameter();
        }

        if (border == 0) {
            border = defaultNode.getBorder();
        }

        if (type == 0) {
            type = defaultNode.getType();
        }

        mxGeometry geom = new mxGeometry();

        geom.setX(xPosition);
        // -1 * yPosition because for scilab (0,0) is bottom left
        geom.setY(-yPosition);
        geom.setHeight(diam);
        geom.setWidth(diam);

        tempNode.setGeometry(geom);
        tempNode.setBorder(border);
        tempNode.setType(type);
        tempNode.setNodeName(nodeName);
        /* TODO note that the width is not the same in metanet and jgraph */
        tempNode.setStyle("strokeWidth=" + border + ";node" + colorStyle);

        tempNode.setValue(getDisplayedValue(graphics, nodeNumber));

        return tempNode;
    }

    /**
     * Return a displayed value according to the graphics.display field.
     * 
     * @param graphics
     *            any egraphics structure (node or edge)
     * @param currentObjectIndex
     *            the selected object (node or edge)
     * @return a display-able object (usually a String)
     */
    private static Object getDisplayedValue(final ScilabMList graphics, final int currentObjectIndex) {
        final int displayIndex = 1;

        /*
         * Pre condition, assert that display is a single string
         */
        final ScilabType displayType = graphics.get(displayIndex);
        if (!(displayType instanceof ScilabString) || displayType.getHeight() * displayType.getWidth() != 1) {
            return null;
        }

        final String display = ((ScilabString) displayType).getData()[0][0];
        final ScilabType field = graphics.getMListFields().get(display);

        return getFieldValue(field, currentObjectIndex);
    }

    /**
     * Get the field value at the specified index, this method handle ScilabType
     * cast.
     * 
     * @param field
     *            the field
     * @param currentFieldNumber
     *            the current field value
     * @return
     */
    private static final Object getFieldValue(final ScilabType field, final int currentFieldNumber) {
        Object value = null;

        if (field instanceof ScilabDouble) {
            value = Double.valueOf(((ScilabDouble) field).getRealPart()[0][currentFieldNumber]);
        } else if (field instanceof ScilabString) {
            value = ((ScilabString) field).getData()[0][currentFieldNumber];
        }

        return value;
    }

    /**
     * 
     * @param nodes
     */

    private static void fillDefaultNode(ScilabMList nodes) {

        ScilabTList defaultValues = (ScilabTList) ((ScilabMList) nodes.get(2)).get(2);
        int defaultNodeType = (int) ((ScilabDouble) defaultValues.get(1)).getRealPart()[0][0];
        double defaultNodeDiam = ((ScilabDouble) defaultValues.get(2)).getRealPart()[0][0];
        double defaultNodeBorder = ((ScilabDouble) defaultValues.get(3)).getRealPart()[0][0];

        ScilabMList nodeData = (ScilabMList) nodes.get(3);
        int numberOfDataField = nodeData.get(0).getWidth() - 1;

        // as the field 0 in a scilabMList is the name of the list
        for (int i = 1; i <= numberOfDataField; i++) {

            String fieldName = ((ScilabString) nodeData.get(0)).getData()[0][i];

            if (nodeData.get(i) instanceof ScilabDouble) {
                double value = 0.0;
                defaultNode.addDataField(fieldName, value);
            }

        }

        if (((ScilabDouble) defaultValues.get(4)).getHeight() == 3) {
            double[][] rgbColor = ((ScilabDouble) defaultValues.get(4)).getRealPart();
            Color defaultNodeColor = new Color((int) rgbColor[0][0], (int) rgbColor[1][0], (int) rgbColor[2][0]);
            defaultNode.setStyle("strokeWidth=" + defaultNodeBorder + ";node;strokeColor=#" + Integer.toHexString(defaultNodeColor.getRGB()).substring(2));
        } else {

            defaultNode.setStyle("strokeWidth=" + defaultNodeBorder + ";node;");
        }

        defaultNode.setType(defaultNodeType);
        defaultNode.setBorder(defaultNodeBorder);
        defaultNode.setDiameter(defaultNodeDiam);
        defaultNode.setGeometry(new mxGeometry(0, 0, defaultNodeDiam, defaultNodeDiam));
    }

    /**
     * 
     * @param nodeNumber
     * @param nodes
     * @return
     * @throws WrongStructureException
     * @throws WrongTypeException
     */

    private static BasicEdge fillEdgeStructure(int edgeNumber, ScilabMList edges, List<BasicNode> nodes) throws WrongStructureException, WrongTypeException {
        BasicEdge tempEdge = new BasicEdge();

        String colorStyle = "";

        int nodeSourceNumber = (int) ((ScilabDouble) edges.get(1)).getRealPart()[0][edgeNumber] - 1;
        int nodeTargetNumber = (int) ((ScilabDouble) edges.get(2)).getRealPart()[0][edgeNumber] - 1;
        ScilabMList graphics = (ScilabMList) edges.get(3);

        int currentFieldNumber = 4;
        String edgeName = ((ScilabString) graphics.get(currentFieldNumber)).getData()[0][edgeNumber];
        currentFieldNumber++;

        if (graphics.getWidth() == 11) {
            if (graphics.get(currentFieldNumber) instanceof ScilabString) {
                final String id = ((ScilabString) graphics.get(currentFieldNumber)).getData()[0][edgeNumber];
                if (id != null && !id.isEmpty()) {
                    tempEdge.setId(id);
                }
            }
            currentFieldNumber++;
        }

        // width
        double edgeWidth = ((ScilabDouble) graphics.get(currentFieldNumber)).getRealPart()[0][edgeNumber];
        currentFieldNumber++;
        if (edgeWidth == 0) {
            edgeWidth = defaultEdge.getStrokeWidth();
        }

        Color currentEdgeColor;

        if (((ScilabDouble) graphics.get(currentFieldNumber)).getHeight() == 3) {
            double[][] rgbColor = ((ScilabDouble) graphics.get(currentFieldNumber)).getRealPart();
            currentEdgeColor = new Color((int) rgbColor[0][edgeNumber], (int) rgbColor[1][edgeNumber], (int) rgbColor[2][edgeNumber]);

        } else {
            // old structure store as the scilab color number
            double[][] scilabColor = ((ScilabDouble) graphics.get(currentFieldNumber)).getRealPart();
            if ((int) scilabColor[0][edgeNumber] != 0) {
                float[] rgb = scilab_to_rgbColor.getScilabColor((int) (scilabColor[0][edgeNumber] - 1));
                currentEdgeColor = new Color(rgb[0], rgb[1], rgb[2]);
            } else {
                currentEdgeColor = Color.BLACK;
            }
        }
        colorStyle = ";strokeColor=#" + Integer.toHexString(currentEdgeColor.getRGB()).substring(2);

        ScilabMList edgeData = (ScilabMList) edges.get(4);
        int numberOfDataField = edgeData.get(0).getWidth() - 1;

        // as the field 0 in a scilabMList is the name of the list
        for (int i = 1; i <= numberOfDataField; i++) {

            String fieldName = ((ScilabString) edgeData.get(0)).getData()[0][i];

            if (edgeData.get(i) instanceof ScilabDouble) {
                double value = ((ScilabDouble) edgeData.get(i)).getRealPart()[0][edgeNumber];
                tempEdge.addDataField(fieldName, value);
            }

        }

        BasicNode sourceNode = nodes.get(nodeSourceNumber);
        BasicNode targetNode = nodes.get(nodeTargetNumber);

        tempEdge.setEdgeName(edgeName);
        tempEdge.setSource(sourceNode);
        tempEdge.setTarget(targetNode);
        tempEdge.setStrokeWidth(edgeWidth);

        String style = "defaultEdge";
        if (directed) {
            style = "directedEdge";
        }

        tempEdge.setStyle(style + ";strokeWidth=" + edgeWidth + colorStyle);
        tempEdge.setValue(getDisplayedValue(graphics, edgeNumber));

        // TODO to fill
        return tempEdge;
    }

    private static void fillDefaultEdge(ScilabMList edges) {
        ScilabTList defaultValues = (ScilabTList) ((ScilabMList) edges.get(3)).get(2);
        double defaultEdgeWdith = ((ScilabDouble) defaultValues.get(1)).getRealPart()[0][0];

        ScilabMList edgeData = (ScilabMList) edges.get(4);
        int numberOfDataField = edgeData.get(0).getWidth() - 1;

        // as the field 0 in a scilabMList is the name of the list
        for (int i = 1; i <= numberOfDataField; i++) {

            String fieldName = ((ScilabString) edgeData.get(0)).getData()[0][i];

            if (edgeData.get(i) instanceof ScilabDouble) {
                double value = 0.0;
                defaultEdge.addDataField(fieldName, value);
            }
        }

        String style = "defaultEdge";
        if (directed) {
            style = "directedEdge";
        }

        if (((ScilabDouble) defaultValues.get(2)).getHeight() == 3) {
            double[][] rgbColor = ((ScilabDouble) defaultValues.get(2)).getRealPart();
            Color defaultNodeColor = new Color((int) rgbColor[0][0], (int) rgbColor[1][0], (int) rgbColor[2][0]);
            defaultEdge.setStyle(style + ";strokeWidth=" + defaultEdgeWdith + ";node;strokeColor=#"
                    + Integer.toHexString(defaultNodeColor.getRGB()).substring(2));
        } else {
            defaultEdge.setStyle(style + ";strokeWidth=" + defaultEdgeWdith);
        }

        defaultEdge.setStrokeWidth(defaultEdgeWdith);
    }

    private static HashMap<String, Object> fillDiagrammProperties(ScilabTList params) {
        HashMap<String, Object> diagramProperties = new HashMap<String, Object>();

        diagramProperties.put("version", ((ScilabString) params.get(2)).getData()[0][0]);
        diagramProperties.put("title", ((ScilabString) params.get(2)).getData()[0][0]);

        return diagramProperties;
    }

    private static void generateScilab2RGBColor() {
        Figure f = GraphicModel.getFigureModel();
        scilab_to_rgbColor = f.getColorMap();
    }

    private static class GraphReaderException extends Exception {
    };

    private static class WrongTypeException extends GraphReaderException {
    };

    private static class WrongStructureException extends GraphReaderException {
    };
}
