/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.io;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.scilab.modules.javasci.JavasciException;
import org.scilab.modules.javasci.Scilab;
import org.scilab.modules.metanet.edge.BasicEdge;
import org.scilab.modules.metanet.graph.MetanetDiagram;
import org.scilab.modules.metanet.node.BasicNode;
import org.scilab.modules.types.ScilabDouble;
import org.scilab.modules.types.ScilabList;
import org.scilab.modules.types.ScilabMList;
import org.scilab.modules.types.ScilabString;
import org.scilab.modules.types.ScilabTList;

import com.mxgraph.util.mxConstants;

public class GraphWriter {

    /**
     * Constructor (MUST NOT BE USED !!!)
     */
    private GraphWriter() {

    }

    /**
     * Main writing function
     * 
     * @param diagram
     *            diagram to save
     * 
     * @return true if file created successfully
     */
    public static boolean writeDiagramToScilab(MetanetDiagram diagram) {
        ScilabTList data = getDiagramGraphList(diagram);
        try {
            Scilab.putInCurrentScilabSession("g", data);
        } catch (JavasciException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * Create a Scilab TList from diagram graph
     * 
     * @return the TList
     */
    private static ScilabTList getDiagramGraphList(MetanetDiagram diagram) {
        String[] optionsFields = { "graph", "version", "name", "directed", "nodes", "edges" };

        ScilabTList data = new ScilabTList(optionsFields);

        ScilabString version = new ScilabString(diagram.getVersion());
        ScilabString name = new ScilabString(diagram.getTitle());
        ScilabDouble directed;
        if (diagram.isDirected()) {
            directed = new ScilabDouble(1.0);
        } else {
            directed = new ScilabDouble(0.0);
        }

        final int nbObjs = diagram.getModel().getChildCount(diagram.getDefaultParent());

        final List<BasicNode> nodesList = new ArrayList<BasicNode>(nbObjs);
        final List<BasicEdge> edgesList = new ArrayList<BasicEdge>(nbObjs);

        for (int i = 0; i < nbObjs; i++) {
            Object currentObject = diagram.getModel().getChildAt(diagram.getDefaultParent(), i);
            if (currentObject instanceof BasicNode) {
                ((BasicNode) currentObject).setOrdering(nodesList.size());
                nodesList.add((BasicNode) currentObject);
            } else if (currentObject instanceof BasicEdge) {
                ((BasicEdge) currentObject).setOrdering(edgesList.size());
                edgesList.add((BasicEdge) currentObject);
            }
        }

        ScilabMList nodes = getDiagramNodes(diagram, nodesList);
        ScilabMList edges = getDiagramEdges(diagram, edgesList);

        data.add(version);
        data.add(name);
        data.add(directed);
        data.add(nodes);
        data.add(edges);

        return data;
    }

    /**
     * Create a Scilab Mlist from the graph nodes
     * 
     * @return the Mlist
     */
    private static ScilabMList getDiagramNodes(MetanetDiagram diagram, List<BasicNode> nodesList) {
        String[] optionsFields = { "nodes", "number", "graphics", "data" };
        ScilabMList data = new ScilabMList(optionsFields);

        int numberOfNodes = nodesList.size();
        ScilabMList graphics;
        ScilabMList nodeData;

        graphics = getDiagramNodesGraphics(diagram, nodesList);
        nodeData = getDiagramNodesData(nodesList, diagram.getDefaultNode());

        data.add(new ScilabDouble(numberOfNodes));
        data.add(graphics);
        data.add(nodeData); // data

        return data;
    }

    /**
     * Create a Scilab Mlist from the graph edges
     * 
     * @return the Mlist
     */

    private static ScilabMList getDiagramEdges(MetanetDiagram diagram, List<BasicEdge> edgesList) {
        String[] optionsFields = { "edges", "tail", "head", "graphics", "data" };
        ScilabMList data = new ScilabMList(optionsFields);

        double[][] tails = new double[1][edgesList.size()];
        double[][] heads = new double[1][edgesList.size()];

        int numberOfEdges = edgesList.size();
        for (int i = 0; i < numberOfEdges; i++) {
            BasicEdge currentEdge = edgesList.get(i);

            tails[0][i] = ((BasicNode) currentEdge.getSource()).getOrdering() + 1;
            heads[0][i] = ((BasicNode) currentEdge.getTarget()).getOrdering() + 1;
        }

        ScilabMList graphics = getDiagramEdgesGraphics(diagram, edgesList);
        ScilabMList edgesData = getDiagramEdgesData(edgesList, diagram.getDefaultEdge());

        data.add(new ScilabDouble(tails));
        data.add(new ScilabDouble(heads));
        data.add(graphics);
        data.add(edgesData);

        return data;
    }

    /**
     * Create a Scilab Mlist from the modes data
     * 
     * @return the Mlist
     */

    private static ScilabMList getDiagramNodesData(List<BasicNode> nodesList, BasicNode defaultNode) {

        String[] dataFields = defaultNode.getDataFieldsName();
        int numberOfNodes = nodesList.size();

        ArrayList<String> options = new ArrayList<String>();
        options.add("nodedata");
        for (int i = 0; i < dataFields.length; ++i) {
            options.add(dataFields[i]);
        }

        ScilabMList data = new ScilabMList(options.toArray(new String[0]));
        for (int i = 0; i < dataFields.length; ++i) {

            String tempFieldName = dataFields[i];

            double[][] tempData = new double[1][numberOfNodes];

            for (int j = 0; j < numberOfNodes; ++j) {
                tempData[0][j] = Double.valueOf(nodesList.get(j).getDataFieldsValue(tempFieldName).toString());
            }

            data.add(new ScilabDouble(tempData));
        }

        return data;
    }

    /**
     * Create a Scilab Mlist from the nodes graphics
     * 
     * @return the Mlist
     */

    private static ScilabMList getDiagramNodesGraphics(MetanetDiagram diagram, List<BasicNode> nodesList) {
        String[] optionsFields = { "ngraphic", "display", "defaults", "name", "id", "x", "y", "type", "diam", "border", "colors", "font", "displaymode" };
        ScilabMList data = new ScilabMList(optionsFields);

        ScilabString display = new ScilabString("nothing"); // TODO to complete

        BasicNode defaultNode = diagram.getDefaultNode();
        ScilabTList defaults = getDiagramNodeDefault(diagram);

        String[][] names = new String[1][nodesList.size()];
        String[][] ids = new String[1][nodesList.size()];
        double[][] xCoordinates = new double[1][nodesList.size()];
        double[][] yCoordinates = new double[1][nodesList.size()];
        double[][] types = new double[1][nodesList.size()];
        double[][] diameters = new double[1][nodesList.size()];
        double[][] borders = new double[1][nodesList.size()];
        double[][] colors = new double[3][nodesList.size()];
        double[][] fonts = new double[3][nodesList.size()];

        int numberOfNodes = nodesList.size();
        for (int i = 0; i < numberOfNodes; i++) {
            BasicNode currentNode = nodesList.get(i);
            names[0][i] = currentNode.getNodeName();
            // id
            ids[0][i] = currentNode.getId();

            // coordinate
            xCoordinates[0][i] = currentNode.getGeometry().getX();
            yCoordinates[0][i] = -currentNode.getGeometry().getY();
            // type
            if (currentNode.getType() == defaultNode.getType()) {
                types[0][i] = 0.0;
            } else {
                types[0][i] = currentNode.getType();
            }
            // diameter
            if (currentNode.getDiameter() == defaultNode.getDiameter()) {
                diameters[0][i] = 0.0;
            } else {
                diameters[0][i] = currentNode.getDiameter();
            }
            // border
            if (currentNode.getBorder() == defaultNode.getBorder()) {
                borders[0][i] = 0.0;
            } else {
                borders[0][i] = currentNode.getBorder();
            }
            // color
            Color currentNodeColor = Color.decode((String) diagram.getCellStyle(currentNode).get(mxConstants.STYLE_STROKECOLOR));
            colors[0][i] = currentNodeColor.getRed();
            colors[1][i] = currentNodeColor.getGreen();
            colors[2][i] = currentNodeColor.getBlue();
            // font
            fonts[0][i] = 0;
            fonts[1][i] = 0;
            fonts[2][i] = 0;
            // display mode
        }

        data.add(display);
        data.add(defaults);
        data.add(new ScilabString(names));
        data.add(new ScilabString(ids));
        data.add(new ScilabDouble(xCoordinates));
        data.add(new ScilabDouble(yCoordinates));
        data.add(new ScilabDouble(types));
        data.add(new ScilabDouble(diameters));
        data.add(new ScilabDouble(borders));
        data.add(new ScilabDouble(colors));
        data.add(new ScilabDouble(fonts));
        data.add(new ScilabString("center"));

        return data;
    }

    /**
     * Create a Scilab Tlist from the default node
     * 
     * @return the Tlist
     */

    private static ScilabTList getDiagramNodeDefault(MetanetDiagram diagram) {
        String[] optionsFields = { "nodedefs", "type", "diam", "border", "colors", "font" };
        ScilabTList data = new ScilabTList(optionsFields);

        BasicNode defaultNode = diagram.getDefaultNode();

        ScilabDouble type = new ScilabDouble(defaultNode.getType());
        ScilabDouble diam = new ScilabDouble(defaultNode.getDiameter());
        ScilabDouble border = new ScilabDouble(defaultNode.getBorder());
        ScilabDouble color = new ScilabDouble();
        Color defaultColor = Color.decode((String) diagram.getCellStyle(defaultNode).get(mxConstants.STYLE_STROKECOLOR));
        color.setRealPart(new double[][] { { defaultColor.getRed() }, { defaultColor.getGreen() }, { defaultColor.getBlue() } });
        ScilabDouble font = new ScilabDouble();
        font.setRealPart(new double[][] { { 12 }, { 6 }, { -1 } }); // TODO
                                                                    // replace
                                                                    // this

        data.add(type);
        data.add(diam);
        data.add(border);
        data.add(color);
        data.add(font);

        return data;
    }

    /**
     * Create a Scilab Mlist from the edges graphics
     * 
     * @return the Mlist
     */

    private static ScilabMList getDiagramEdgesGraphics(MetanetDiagram diagram, List<BasicEdge> edgesList) {
        String[] optionsFields = { "egraphic", "display", "defaults", "profiles", "name", "id", "width", "colors", "font", "profile_index", "displaymode" };
        ScilabMList data = new ScilabMList(optionsFields);

        ScilabString display = new ScilabString("nothing"); // TODO to complete

        BasicEdge defaultEdge = diagram.getDefaultEdge();
        ScilabTList defaults = getDiagramEdgeDefault(diagram);

        ScilabList profiles = new ScilabList();

        String[][] names = new String[1][edgesList.size()];
        String[][] ids = new String[1][edgesList.size()];
        double[][] widths = new double[1][edgesList.size()];
        double[][] foregrounds = new double[3][edgesList.size()];
        double[][] fonts = new double[3][edgesList.size()];
        double[][] profile_indexes = new double[1][edgesList.size()];

        profiles.add(new ScilabDouble(1.2));

        int numberOfEdges = edgesList.size();
        for (int i = 0; i < numberOfEdges; i++) {
            BasicEdge currentEdge = edgesList.get(i);
            // name
            names[0][i] = currentEdge.getEdgeName();
            // id
            ids[0][i] = currentEdge.getId();
            // width
            if (currentEdge.getStrokeWidth() == defaultEdge.getStrokeWidth()) {
                widths[0][i] = 0.0;
            } else {
                widths[0][i] = currentEdge.getStrokeWidth();
            }
            // foregrounds
            Color edgeColor = Color.decode((String) diagram.getCellStyle(currentEdge).get(mxConstants.STYLE_STROKECOLOR));
            foregrounds[0][i] = edgeColor.getRed(); // TODO to change
            foregrounds[1][i] = edgeColor.getGreen();
            foregrounds[2][i] = edgeColor.getBlue();
            // font
            fonts[0][i] = 0;
            fonts[1][i] = 0;
            fonts[2][i] = 0;
            // profile_index
            profile_indexes[0][i] = 0;
        }

        data.add(display);
        data.add(defaults);
        data.add(profiles);// profiles
        data.add(new ScilabString(names));// name
        data.add(new ScilabString(ids));// id
        data.add(new ScilabDouble(widths));// width
        data.add(new ScilabDouble(foregrounds));// foreground
        data.add(new ScilabDouble(fonts));// font
        data.add(new ScilabDouble(profile_indexes));// profile index
        data.add(new ScilabString("tangeant"));// display mode TODO to complete

        return data;
    }

    /**
     * Create a Scilab Tlist from the default node
     * 
     * @return the Tlist
     */

    private static ScilabTList getDiagramEdgeDefault(MetanetDiagram diagram) {
        String[] optionsFields = { "edgedefs", "width", "colors", "font", "profile_index" };
        ScilabTList data = new ScilabTList(optionsFields);

        BasicEdge defaultEdge = diagram.getDefaultEdge();

        ScilabDouble width = new ScilabDouble(defaultEdge.getStrokeWidth());

        ScilabDouble foreground = new ScilabDouble();
        Color defaultColor = Color.decode((String) diagram.getCellStyle(defaultEdge).get(mxConstants.STYLE_STROKECOLOR));
        foreground.setRealPart(new double[][] { { defaultColor.getRed() }, { defaultColor.getBlue() }, { defaultColor.getGreen() } });

        ScilabDouble font = new ScilabDouble();
        font.setRealPart(new double[][] { { 12 }, { 6 }, { -1 } }); // TODO
                                                                    // replace
                                                                    // this

        data.add(width);
        data.add(foreground); // foreground
        data.add(font); // font
        data.add(new ScilabDouble(2.0)); // profile_index

        return data;
    }

    /**
     * Create a Scilab Mlist from the edges data
     * 
     * @return the Mlist
     */

    private static ScilabMList getDiagramEdgesData(List<BasicEdge> edgesList, BasicEdge defaultEdge) {
        String[] dataFields = defaultEdge.getDataFieldsName();
        int numberOfNodes = edgesList.size();

        ArrayList<String> options = new ArrayList<String>();
        options.add("edgedata");
        for (int i = 0; i < dataFields.length; ++i) {
            options.add(dataFields[i]);
        }

        ScilabMList data = new ScilabMList(options.toArray(new String[0]));
        for (int i = 0; i < dataFields.length; ++i) {

            String tempFieldName = dataFields[i];

            double[][] tempData = new double[1][numberOfNodes];

            for (int j = 0; j < numberOfNodes; ++j) {
                tempData[0][j] = Double.valueOf(edgesList.get(j).getDataFieldsValue(tempFieldName).toString());
            }

            data.add(new ScilabDouble(tempData));
        }

        return data;
    }
}
