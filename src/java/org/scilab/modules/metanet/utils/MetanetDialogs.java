/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.utils;

import org.scilab.modules.gui.messagebox.ScilabModalDialog;
import org.scilab.modules.gui.messagebox.ScilabModalDialog.IconType;
import org.scilab.modules.metanet.MetanetTab;
import org.scilab.modules.metanet.graph.MetanetDiagram;

public final class MetanetDialogs {

    /**
     * Constructor
     */
    private MetanetDialogs() {

    }

    /**
     * Dialog displayed when saving failed
     * 
     * @param diagram
     *            The associated diagram
     */
    public static void couldNotSaveFile(MetanetDiagram diagram) {
        ScilabModalDialog.show(MetanetTab.get(diagram), MetanetMessages.FAIL_SAVING_DIAGRAM, MetanetMessages.METANET_ERROR, IconType.ERROR_ICON);
    }

    /**
     * Dialog displayed when loading failed
     * 
     * @param diagram
     *            The associated diagram
     */
    public static void couldNotLoadFile(MetanetDiagram diagram) {
        ScilabModalDialog.show(MetanetTab.get(diagram), MetanetMessages.FAIL_LOADING_DIAGRAM, MetanetMessages.METANET_ERROR, IconType.ERROR_ICON);
    }
}
