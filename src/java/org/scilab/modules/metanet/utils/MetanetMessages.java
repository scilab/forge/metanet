/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.utils;

import java.awt.event.MouseEvent;

import javax.swing.SwingUtilities;

import org.scilab.modules.localization.Messages;

/**
 * All localized messages/menus labels used in Metanet
 */
// CSOFF: JavadocVariable
// CSOFF: LineLength
// CSOFF: MultipleStringLiterals

public final class MetanetMessages {

    public static final String METANET = Messages.gettext("Metanet");
    public static final String DOTS = "...";
    public static final String UNTITLED = Messages.gettext("Untitled");

    public static final String DIAGRAM_MODIFIED = Messages.gettext("Diagram has been modified since last save.<br/> Do you want to save it?");
    public static final String FILE_DOESNT_EXIST = Messages.gettext("The file %s doesn't exist\n Do you want to create it?");

    /* File menu */
    public static final String FILE = Messages.gettext("File");
    public static final String NEW = Messages.gettext("New");
    public static final String NEW_DIAGRAM = Messages.gettext("New diagram") + DOTS;
    public static final String OPEN = Messages.gettext("Open") + DOTS;
    public static final String SAVE = Messages.gettext("Save");
    public static final String SAVE_AS = Messages.gettext("Save as") + DOTS;
    public static final String EXPORT = Messages.gettext("Export") + DOTS;
    public static final String PRINT = Messages.gettext("Print") + DOTS;
    public static final String CLOSE = Messages.gettext("Close");
    public static final String QUIT = Messages.gettext("Quit Metanet");
    public static final String RECENT_FILES = Messages.gettext("Recent Files");

    /* Graph Menu */
    public static final String GRAPH = Messages.gettext("Graph");
    public static final String EDIT_DEFAULT_NODE = Messages.gettext("Edit Default Node");
    public static final String EDIT_DEFAULT_EDGE = Messages.gettext("Edit Default Edge");
    public static final String ADD_EDGE_DATA_FIELD = Messages.gettext("Add edge data field");
    public static final String ADD_NODE_DATA_FIELD = Messages.gettext("Add node data field");
    public static final String VIEW_LABELS = Messages.gettext("View labels");

    /* Edit Menu */
    public static final String EDIT = Messages.gettext("Edit");
    public static final String NODE_PARAMETERS = Messages.gettext("Node Parameters") + DOTS;
    public static final String EDGE_PARAMETERS = Messages.gettext("Edge Parameters") + DOTS;

    /* Format menu */
    public static final String FORMAT = Messages.gettext("Format");
    public static final String BORDER_COLOR = Messages.gettext("Border Color");
    public static final String FILL_COLOR = Messages.gettext("Fill Color");
    public static final String CHANGE_EDGE_COLOR = Messages.gettext("Change Edge Color");

    /* Compute menu */
    public static final String COMPUTE = Messages.gettext("Compute");
    public static final String SHORTEST_PATH = Messages.gettext("Shortest Path");
    public static final String CIRCUIT = Messages.gettext("Circuit");
    public static final String SALESMAN = Messages.gettext("Salesman");

    /* View Menu */
    public static final String VIEWPORT = Messages.gettext("Viewport");
    public static final String FIT_DIAGRAM_TO_VIEW = Messages.gettext("Fit diagram to view");

    /* File description */
    public static final String FILE_GRAPH = Messages.gettext("Metanet file");
    public static final String FILE_HDF5 = Messages.gettext("Scilab file");
    public static final String FILE_XGRAPH = Messages.gettext("Xgraph file");
    /* InfoBar messages */
    public static final String EMPTY_INFO = "";
    public static final String SAVING_DIAGRAM = Messages.gettext("Saving diagram" + DOTS);
    public static final String LOADING_DIAGRAM = Messages.gettext("Loading diagram" + DOTS);

    public static final String FAIL_LOADING_DIAGRAM = Messages.gettext("Failed to load Diagram");
    public static final String FAIL_SAVING_DIAGRAM = Messages.gettext("Could not save diagram.");
    public static final String METANET_ERROR = Messages.gettext("Metanet error");

    public static final String OK = Messages.gettext("Ok");
    public static final String CANCEL = Messages.gettext("Cancel");
    public static final String DEFAULT = Messages.gettext("Default");

    public static final String DIAMETER = Messages.gettext("Diameter");
    public static final String NAME = Messages.gettext("Name");

    public static final String USE_NUMBER_OF_EDGES = Messages.gettext("number of edges");

    public static final String NEW_FIELD_NAME = Messages.gettext("Name of the new field");
    public static final String NEW_FIELD_DEFAULT_VALUE = Messages.gettext("Default value for this field");

    public static final String ERR_CONFIG_INVALID = Messages.gettext("The user configuration file (metanet.xml) is invalid.<BR> Switching to the default one.");

    /**
     * This function checks for the popup menu activation under MacOS with Java
     * version 1.5 Related to Scilab bug #5190
     * 
     * @param e
     *            Click event
     * @return true if Java 1.5 and MacOS and mouse clic and ctrl activated
     */
    public static boolean isMacOsPopupTrigger(MouseEvent e) {
        return (SwingUtilities.isLeftMouseButton(e) && e.isControlDown() && (System.getProperty("os.name").toLowerCase().indexOf("mac") != -1) && (System
                .getProperty("java.specification.version").equals("1.5")));
    }
}
