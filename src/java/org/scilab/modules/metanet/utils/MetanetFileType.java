/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.utils;

import java.io.File;
import java.io.IOException;

import org.scilab.modules.action_binding.highlevel.ScilabInterpreterManagement;
import org.scilab.modules.action_binding.highlevel.ScilabInterpreterManagement.InterpreterException;

/**
 * All the filetype recognized by Metanet.
 */
public enum MetanetFileType {
    GRAPH("graph", MetanetMessages.FILE_GRAPH) {
        /**
         * Export the typed file to the HDF5 format.
         * 
         * @param arg0
         *            The GRAPH formatted file
         * @return The HDF5 formatted file
         */
        @Override
        public File exportToHdf5(File arg0) {
            return loadMetanetDiagram(arg0);
        }
    },
    HDF5("h5", MetanetMessages.FILE_HDF5) {
        /**
         * Export the typed file to the HDF5 format. (does nothing there)
         * 
         * @param arg0
         *            The HDF5 formatted file
         * @return The HDF5 formatted file
         */
        @Override
        public File exportToHdf5(File arg0) {
            return arg0;
        }
    },
    XGRAPH("xgraph", MetanetMessages.FILE_XGRAPH), UNKNOW("", "");

    private String extension;
    private String description;

    /**
     * Default constructor
     * 
     * @param extension
     *            file extension (without the dot)
     * @param description
     *            file description
     */
    MetanetFileType(String extension, String description) {
        this.extension = extension;
        this.description = description;
    }

    /**
     * @return the extension prefixed with a dot
     */
    public String getDottedExtension() {
        return "." + extension;
    }

    /**
     * @return the raw extension
     */
    public String getExtension() {
        return extension;
    }

    /**
     * @return the mask of this file
     */
    public String getFileMask() {
        return "*." + getExtension();
    }

    /**
     * @return the file description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Find a filetype by the filename extension
     * 
     * @param theFile
     *            Current file
     * @return The determined filetype
     */
    public static MetanetFileType findFileType(File theFile) {
        int dotPos = theFile.getName().lastIndexOf('.');
        String extension = "";
        MetanetFileType retValue = MetanetFileType.UNKNOW;

        if (dotPos > 0 && dotPos <= theFile.getName().length() - 2) {
            extension = theFile.getName().substring(dotPos + 1);
        }

        for (MetanetFileType currentFileType : MetanetFileType.values()) {
            if (extension.compareToIgnoreCase(currentFileType.extension) == 0) {
                retValue = currentFileType;
                break;
            }
        }

        return retValue;
    }

    /**
     * Convert the file passed as an argument to Hdf5.
     * 
     * @param file
     *            The file to convert
     * @return The created file
     */
    public File exportToHdf5(File file) {
        throw new Error("Not implemented operation");
    }

    /**
     * Get a valid file mask (useable by file selector)
     * 
     * @return A valid file mask
     */
    public static String[] getValidFileMask() {
        String[] result = new String[MetanetFileType.values().length - 1];

        for (int i = 0; i < result.length; i++) {
            result[i] = MetanetFileType.values()[i].getFileMask();
        }

        return result;
    }

    /**
     * Get a valid file description (useable by file selector)
     * 
     * @return A valid file mask
     */
    public static String[] getValidFileDescription() {
        String[] result = new String[MetanetFileType.values().length - 1];

        for (int i = 0; i < result.length; i++) {
            result[i] = MetanetFileType.values()[i].getDescription() + " (*." + MetanetFileType.values()[i].getExtension() + ")";
        }

        return result;
    }

    /**
     * Convert a Metanet diagram (GraphList scilab script) to an hdf5 file
     * 
     * @param filename
     *            The file to execute in scilab.
     * @return The exported data in hdf5.
     */
    public static File loadMetanetDiagram(File filename) {
        File tempOutput = null;
        try {
            tempOutput = File.createTempFile(MetanetFileType.XGRAPH.getExtension(), MetanetFileType.HDF5.getDottedExtension());
            /*********************
             * FIXME I'm a HACK *
             *********************/

            String cmd = "load(\"" + filename.getAbsolutePath() + "\");";
            cmd += "[toto,X,Y,W]= listvarinfile(\"" + filename.getAbsolutePath() + "\");";
            cmd += "GraphList2 = eval(toto);";
            cmd += "export_to_hdf5(\"" + tempOutput.getAbsolutePath() + "\", \"GraphList2\");";
            try {
                ScilabInterpreterManagement.synchronousScilabExec(cmd);
            } catch (InterpreterException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tempOutput;
    }
}
