/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.utils;

import org.scilab.modules.commons.ScilabConstants;

public class MetanetConstants extends ScilabConstants {

    public static final int EDGE_ARC_HEIGHT = 20;

    public static final int REFERENCE_NODE_DIAM = 30;

    public static final String ARC = "arc";

    /* SCI environment */
    /** Path from SCI or SCIHOME to the Metanet configuration directory */
    public static final String METANET_ETC = "/modules/metanet/etc";

}
