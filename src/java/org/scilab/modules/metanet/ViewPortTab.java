package org.scilab.modules.metanet;

import java.util.List;
import java.util.ListIterator;
import java.util.UUID;

import org.scilab.modules.gui.bridge.tab.SwingScilabDockablePanel;
import org.scilab.modules.gui.bridge.window.SwingScilabDockingWindow;
import org.scilab.modules.gui.bridge.window.SwingScilabWindow;
import org.scilab.modules.gui.tabfactory.ScilabTabFactory;
import org.scilab.modules.gui.utils.ClosingOperationsManager;
import org.scilab.modules.gui.utils.WindowsConfigurationManager;
import org.scilab.modules.metanet.configuration.ConfigurationManager;
import org.scilab.modules.metanet.graph.MetanetDiagram;
import org.scilab.modules.metanet.utils.MetanetMessages;

import com.mxgraph.swing.mxGraphOutline;

public final class ViewPortTab extends SwingScilabDockablePanel {
    public static final String DEFAULT_WIN_UUID = "metanet-viewport-default-window";
    public static final String DEFAULT_TAB_UUID = "metanet-viewport-default-tab";

    private ViewPortTab(MetanetDiagram graph, String uuid) {
        super(MetanetMessages.VIEWPORT, uuid);

        graph.setViewPortTab(uuid);
        ;

        initComponents(graph);
    }

    private static class ClosingOperation implements org.scilab.modules.gui.utils.ClosingOperationsManager.ClosingOperation {
        private final MetanetDiagram graph;

        public ClosingOperation(final MetanetDiagram graph) {
            this.graph = graph;
        }

        @Override
        public int canClose() {
            return 1;
        }

        @Override
        public void destroy() {
            graph.setViewPortTab(null);

            final MetanetTab tab = MetanetTab.get(graph);
            tab.setViewportChecked(false);

            graph.setViewPortTab(null);
            ConfigurationManager.getInstance().updateOpenedTab(graph);
        }

        @Override
        public String askForClosing(List<SwingScilabDockablePanel> list) {
            return null;
        }

        @Override
        public void updateDependencies(List<SwingScilabDockablePanel> list, ListIterator<SwingScilabDockablePanel> it) {
        }

    }

    private static class EndedRestoration implements WindowsConfigurationManager.EndedRestoration {
        private final MetanetDiagram graph;

        public EndedRestoration(MetanetDiagram graph) {
            this.graph = graph;
        }

        @Override
        public void finish() {
            ConfigurationManager.getInstance().updateOpenedTab(graph);
        }
    }

    /*
     * Static API for Tabs
     */

    /**
     * Get the viewport for a graph.
     * 
     * @param graph
     *            the graph
     * @return the view port
     */
    public static ViewPortTab get(MetanetDiagram graph) {
        final String uuid = graph.getViewPortTab();
        return (ViewPortTab) ScilabTabFactory.getInstance().getFromCache(uuid);
    }

    /**
     * Restore or create the viewport tab for the graph
     * 
     * @param graph
     *            the graph
     */
    public static void restore(MetanetDiagram graph) {
        restore(graph, true);
    }

    /**
     * Restore or create the viewport tab for the graph
     * 
     * @param graph
     *            the graph
     * @param visible
     *            should the tab should be visible
     */
    public static void restore(final MetanetDiagram graph, final boolean visible) {
        String uuid = graph.getViewPortTab();
        if (uuid == null) {
            uuid = UUID.randomUUID().toString();
        }

        ViewPortTab tab = new ViewPortTab(graph, uuid);
        if (visible) {
            tab.createDefaultWindow().setVisible(true);
            tab.setCurrent();
        }
        ScilabTabFactory.getInstance().addToCache(tab);

        ClosingOperationsManager.registerClosingOperation(tab, new ClosingOperation(graph));
        ClosingOperationsManager.addDependency(MetanetTab.get(graph), tab);

        WindowsConfigurationManager.registerEndedRestoration(tab, new EndedRestoration(graph));
    }

    /*
     * Specific implementation
     */

    private void initComponents(MetanetDiagram graph) {
        final mxGraphOutline outline = new mxGraphOutline(graph.getAsComponent());
        outline.setDrawLabels(true);

        setContentPane(outline);
    }

    private SwingScilabWindow createDefaultWindow() {
        final SwingScilabWindow win;

        final SwingScilabWindow configuration = WindowsConfigurationManager.createWindow(DEFAULT_WIN_UUID, false);
        if (configuration != null) {
            win = configuration;
        } else {
            win = new SwingScilabDockingWindow();
        }

        win.addTab(this);
        return win;
    }
}
