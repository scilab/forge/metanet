/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.actions;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import org.scilab.modules.graph.ScilabGraph;
import org.scilab.modules.graph.actions.base.DefaultAction;
import org.scilab.modules.gui.menuitem.MenuItem;
import org.scilab.modules.metanet.Metanet;
import org.scilab.modules.metanet.utils.MetanetMessages;

public class QuitAction extends DefaultAction {
    private static final long serialVersionUID = 1L;
    public static final String NAME = MetanetMessages.QUIT;
    public static final String SMALL_ICON = "";
    public static final int MNEMONIC_KEY = KeyEvent.VK_Q;
    public static final int ACCELERATOR_KEY = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();

    /**
     * Constructor
     * 
     * @param scilabGraph
     *            associated Metanet diagram
     */
    public QuitAction(ScilabGraph scilabGraph) {
        super(scilabGraph);
    }

    /**
     * Create menu for the Scilab Graph menubar
     * 
     * @param scilabGraph
     *            associated Xcos diagram
     * @return the menu
     */
    public static MenuItem createMenu(ScilabGraph scilabGraph) {
        return createMenu(scilabGraph, QuitAction.class);
    }

    /**
     * Associated action
     * 
     * @see org.scilab.modules.graph.actions.DefaultAction#doAction()
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Metanet.closeSession();
    }
}
