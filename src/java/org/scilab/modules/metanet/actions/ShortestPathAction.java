/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.actions;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import org.scilab.modules.action_binding.highlevel.ScilabInterpreterManagement;
import org.scilab.modules.action_binding.highlevel.ScilabInterpreterManagement.InterpreterException;
import org.scilab.modules.graph.ScilabGraph;
import org.scilab.modules.graph.actions.base.DefaultAction;
import org.scilab.modules.gui.menuitem.MenuItem;
import org.scilab.modules.javasci.JavasciException;
import org.scilab.modules.javasci.Scilab;
import org.scilab.modules.localization.Messages;
import org.scilab.modules.metanet.edge.BasicEdge;
import org.scilab.modules.metanet.graph.MetanetDiagram;
import org.scilab.modules.metanet.io.GraphWriter;
import org.scilab.modules.metanet.node.BasicNode;
import org.scilab.modules.metanet.utils.MetanetConstants;
import org.scilab.modules.metanet.utils.MetanetMessages;
import org.scilab.modules.types.ScilabString;
import org.scilab.modules.types.ScilabType;

import com.mxgraph.model.mxGraphModel;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource;
import com.mxgraph.view.mxGraphSelectionModel;

public class ShortestPathAction extends DefaultAction {

    private static final long serialVersionUID = 1L;

    private static final String SOURCE_AND_TARGET_SELECTION = Messages.gettext("select two nodes (source node first then a target node)");
    private static final String TARGET_SELECTION = Messages.gettext("select the target node");
    private static final String TOO_MANY_NODES = Messages.gettext("Too many node selected");

    public static final String NAME = MetanetMessages.SHORTEST_PATH;
    public static final String SMALL_ICON = "";
    public static final int MNEMONIC_KEY = 0;
    public static final int ACCELERATOR_KEY = 0;

    private ArrayList<String> fieldNamesList;
    private ArrayList<JRadioButton> radioButtonList;
    private MetanetDiagram diagram;
    private JFrame mainFrame;
    private GridBagConstraints gbc;

    private static BasicNode sourceNode = null;
    private static BasicNode targetNode = null;
    private static boolean windowAlreadyExist;

    /**
     * Constructor
     * 
     * @param scilabGraph
     *            associated Scilab Graph
     */
    public ShortestPathAction(ScilabGraph scilabGraph) {
        super(scilabGraph);
    }

    /**
     * Create a menu to add in Scilab Graph menu bar
     * 
     * @param scilabGraph
     *            associated Scilab Graph
     * @return the menu
     */
    public static MenuItem createMenu(ScilabGraph scilabGraph) {
        return createMenu(scilabGraph, ShortestPathAction.class);
    }

    /**
     * Create a button to add in Scilab Graph tool bar
     * 
     * @param scilabGraph
     *            associated Scilab Graph
     * @return the button
     */
    public static JButton createButton(ScilabGraph scilabGraph) {
        return createButton(scilabGraph, ShortestPathAction.class);
    }

    /**
     * Open file action
     * 
     * @see org.scilab.modules.graph.actions.DefaultAction#doAction()
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        /** Avoid to have this window created two times */
        if (windowAlreadyExist) {
            mainFrame.setVisible(true);
            return;
        }

        diagram = ((MetanetDiagram) getGraph(e));
        diagram.info(SOURCE_AND_TARGET_SELECTION);
        sourceNode = targetNode = null;

        final mxGraphSelectionModel selectionModel = diagram.getSelectionModel();
        selectionModel.addListener(mxEvent.CHANGE, new mxEventSource.mxIEventListener() {

            @Override
            public void invoke(Object arg0, mxEventObject arg1) {
                if (selectionModel.isEmpty()) {
                    diagram.info(SOURCE_AND_TARGET_SELECTION);
                    sourceNode = targetNode = null;
                } else if (selectionModel.size() == 1) {
                    final Object cell = selectionModel.getCell();

                    if (cell instanceof BasicNode) {

                        if (sourceNode == null && targetNode == null) {
                            sourceNode = (BasicNode) cell;
                            diagram.info(TARGET_SELECTION);
                        } else if (sourceNode != null && targetNode == null) {
                            targetNode = (BasicNode) cell;
                        }
                    }
                } else if (selectionModel.size() == 2) {
                    Object[] nodes = selectionModel.getCells();
                    if (nodes[0] instanceof BasicNode && nodes[1] instanceof BasicNode) {
                        // Everything is fine, perform action
                        sourceNode = (BasicNode) nodes[0];
                        targetNode = (BasicNode) nodes[1];
                    } else {
                        diagram.info(SOURCE_AND_TARGET_SELECTION);
                        sourceNode = targetNode = null;
                    }
                } else {
                    diagram.info(TOO_MANY_NODES);
                    sourceNode = targetNode = null;
                }

                if (sourceNode != null && targetNode != null) {
                    selectionModel.removeListener(this);
                    updateUI(true);
                    chooseTypeForShortestPath();
                }
            }
        });
    }

    public void chooseTypeForShortestPath() {

        MetanetDiagram diagram = (MetanetDiagram) getGraph(null);
        BasicEdge defaultEdge = diagram.getDefaultEdge();

        Icon scilabIcon = new ImageIcon(System.getenv("SCI") + "/modules/gui/images/icons/scilab.png");
        Image imageForIcon = ((ImageIcon) scilabIcon).getImage();

        mainFrame = new JFrame();
        windowAlreadyExist = true;

        mainFrame.setLayout(new GridBagLayout());
        mainFrame.setIconImage(imageForIcon);

        gbc = new GridBagConstraints();

        /* generate all the input from the data inside the default Node */
        String[] fieldsName = defaultEdge.getDataFieldsName();
        int numberOfFields = fieldsName.length;

        fieldNamesList = new ArrayList<String>(numberOfFields + 1);
        radioButtonList = new ArrayList<JRadioButton>(numberOfFields + 1);
        ButtonGroup field2UseGroup = new ButtonGroup();

        JRadioButton useNumberOfArcRadio = new JRadioButton();
        useNumberOfArcRadio.setSelected(true);
        addInputLine(0, useNumberOfArcRadio, MetanetMessages.USE_NUMBER_OF_EDGES, null, null);
        field2UseGroup.add(useNumberOfArcRadio);
        fieldNamesList.add(MetanetConstants.ARC);
        radioButtonList.add(useNumberOfArcRadio);

        for (int i = 0; i < numberOfFields; i++) {
            String currentFieldValue = defaultEdge.getDataFieldsValue(fieldsName[i]).toString();

            JRadioButton useForShortestPathRadio = new JRadioButton();
            useForShortestPathRadio.setSelected(false);
            field2UseGroup.add(useForShortestPathRadio);
            addInputLine(i + 1, useForShortestPathRadio, fieldsName[i], null, null);

            /**/
            fieldNamesList.add(fieldsName[i]);
            radioButtonList.add(useForShortestPathRadio);
        }

        /* ok cancel and reset to default button */

        JButton okButton = new JButton(MetanetMessages.OK);
        JButton cancelButton = new JButton(MetanetMessages.CANCEL);

        numberOfFields++;
        gbc.gridx = 1;
        gbc.gridy = numberOfFields;
        gbc.gridheight = gbc.gridwidth = 1;
        gbc.weightx = 1.;
        gbc.fill = GridBagConstraints.NONE;
        gbc.insets = new Insets(5, 0, 10, 5);
        mainFrame.add(okButton, gbc);

        gbc.gridx = 2;
        gbc.weightx = 0.;
        gbc.insets = new Insets(5, 0, 10, 10);
        mainFrame.add(cancelButton, gbc);

        // OK Listener
        okButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                String fieldName = "";

                if (radioButtonList.get(0).isSelected()) {
                    fieldName = MetanetConstants.ARC;
                    ; // will use number of edge to compute the shortest path
                }

                for (int i = 1; i < fieldNamesList.size(); ++i) {
                    if (radioButtonList.get(i).isSelected()) {
                        fieldName = fieldNamesList.get(i);
                        break;
                    }
                }

                callScilabShortestPath(sourceNode, targetNode, fieldName);
                sourceNode = null;
                targetNode = null;
                windowAlreadyExist = false;

                mainFrame.dispose();
            }

        });

        // Cancel Listener
        cancelButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                windowAlreadyExist = false;
                updateUI(false);
                mainFrame.dispose();
            }
        });

        mainFrame.addWindowListener(new WindowListener() {
            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                windowAlreadyExist = false;
                mainFrame.dispose();
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowOpened(WindowEvent e) {
            }
        });

        mainFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        mainFrame.setTitle(MetanetMessages.EDIT_DEFAULT_NODE);
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(diagram.getAsComponent());
        mainFrame.setVisible(true);

    }

    /**
     * 
     * @param sourceNode
     * @param targetNode
     */

    public void callScilabShortestPath(BasicNode sourceNode, BasicNode targetNode, String fieldUsed) {
        updateUI(true);

        GraphWriter.writeDiagramToScilab(diagram);

        final String command = "sourceIndex = nodes_ids_2_indexes(\"" + sourceNode.getId() + "\",g);" + "targetIndex = nodes_ids_2_indexes(\""
                + targetNode.getId() + "\",g);" + "edgeIds = edges_2_ids(shortest_path(sourceIndex,targetIndex,g,\"" + fieldUsed + "\"),g);";
        try {
            ScilabInterpreterManagement.asynchronousScilabExec(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent arg0) {
                    ScilabType data;
                    try {
                        data = Scilab.getInCurrentScilabSession("edgeIds");
                    } catch (JavasciException e) {
                        updateUI(false);
                        return;
                    }

                    // if scilab doesn't return an empty matrix
                    if (data instanceof ScilabString) {
                        final ScilabString dataPath = (ScilabString) data;

                        String[] path = dataPath.getData()[0];
                        BasicEdge[] pathEdges = new BasicEdge[path.length];

                        for (int i = 0; i < path.length; ++i) {
                            pathEdges[i] = (BasicEdge) ((mxGraphModel) diagram.getModel()).getCell(path[i]);
                        }

                        diagram.setSelectionCells(pathEdges);
                        diagram.refresh();
                    }

                    updateUI(false);
                }
            }, command);
        } catch (InterpreterException e1) {
            e1.printStackTrace();
            updateUI(false);
        }
    }

    /**
     * Update the UI depending on the action selected or not
     * 
     * @param started
     *            the started status
     */
    public void updateUI(boolean started) {

        if (started) {
            ((MetanetDiagram) getGraph(null)).info("searching for shortest path...");
        } else {
            ((MetanetDiagram) getGraph(null)).info(MetanetMessages.EMPTY_INFO);
        }

        ((MetanetDiagram) getGraph(null)).setReadOnly(started);

    }

    /**
	 * 
	 *
	 */

    private void addInputLine(int indexY, JRadioButton useForShortestPath, String labelName, JTextField inputField, String value) {
        gbc.gridy = indexY;

        /* create label */
        gbc.gridx = 0;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.NONE;
        gbc.insets = new Insets(0, 10, 0, 0);

        mainFrame.add(useForShortestPath, gbc);

        /* create label */
        gbc.gridx = 1;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.NONE;
        gbc.insets = new Insets(0, 10, 0, 0);

        JLabel fieldNameLabel = new JLabel(labelName, SwingConstants.TRAILING);

        mainFrame.add(fieldNameLabel, gbc);

        /* create input field */

        if (inputField != null) {
            gbc.gridx = 2;
            gbc.gridheight = 1;
            gbc.gridwidth = GridBagConstraints.REMAINDER;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.insets = new Insets(5, 10, 0, 10);

            inputField.setText(value);

            mainFrame.add(inputField, gbc);
        }

        // changeStyleColorButton.addActionListener(changeColorListener);

    }

}