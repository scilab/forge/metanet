/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2012 - Scilab Enterprises - Clement DAVID
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
package org.scilab.modules.metanet.actions;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;

import org.scilab.modules.graph.ScilabGraph;
import org.scilab.modules.graph.actions.base.DefaultAction;
import org.scilab.modules.gui.checkboxmenuitem.CheckBoxMenuItem;
import org.scilab.modules.metanet.graph.MetanetDiagram;
import org.scilab.modules.metanet.utils.MetanetMessages;

public class ViewLabelsAction extends DefaultAction {
    public static final String NAME = MetanetMessages.VIEW_LABELS;
    public static final String SMALL_ICON = "";
    public static final int MNEMONIC_KEY = 0;
    public static final int ACCELERATOR_KEY = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();

    public ViewLabelsAction(ScilabGraph scilabGraph) {
        super(scilabGraph);
    }

    /**
     * Menu for diagram menubar
     * 
     * @param scilabGraph
     *            associated diagram
     * @return the menu
     */
    public static CheckBoxMenuItem createMenu(ScilabGraph scilabGraph) {
        return createCheckBoxMenu(scilabGraph, ViewLabelsAction.class);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        final MetanetDiagram graph = (MetanetDiagram) getGraph(null);
        graph.setLabelsVisible(!graph.isLabelsVisible());

        graph.getView().clear(graph.getCurrentRoot(), true, true);
        graph.refresh();
    }
}
