/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.actions;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.JButton;

import org.scilab.modules.graph.ScilabGraph;
import org.scilab.modules.graph.actions.base.DefaultAction;
import org.scilab.modules.gui.menuitem.MenuItem;
import org.scilab.modules.metanet.graph.MetanetDiagram;
import org.scilab.modules.metanet.utils.MetanetMessages;

public class SaveAction extends DefaultAction {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    public static final String NAME = MetanetMessages.SAVE;
    public static final String SMALL_ICON = "media-floppy";
    public static final int MNEMONIC_KEY = KeyEvent.VK_S;
    public static final int ACCELERATOR_KEY = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();

    /**
     * Constructor
     * 
     * @param scilabGraph
     *            associated Metanet diagram
     */
    public SaveAction(ScilabGraph scilabGraph) {
        super(scilabGraph);
    }

    /**
     * Create menu for saving
     * 
     * @param scilabGraph
     *            associated Metanet diagram
     * @return the menu
     */
    public static MenuItem createMenu(ScilabGraph scilabGraph) {
        return createMenu(scilabGraph, SaveAction.class);
    }

    /**
     * Create toolbar button for saving
     * 
     * @param scilabGraph
     *            associated Metanet diagram
     * @return the button
     */
    public static JButton createButton(ScilabGraph scilabGraph) {
        return createButton(scilabGraph, SaveAction.class);
    }

    /**
     * @param e
     *            parameter
     * @see org.scilab.modules.graph.actions.base.DefaultAction#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        ((MetanetDiagram) getGraph(null)).saveDiagram();
    }

}
