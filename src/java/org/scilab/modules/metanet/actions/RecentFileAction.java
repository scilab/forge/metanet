/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - DIGITEO - Vincent COUVERT
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.actions;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.scilab.modules.graph.actions.base.DefaultAction;
import org.scilab.modules.gui.menuitem.MenuItem;
import org.scilab.modules.gui.menuitem.ScilabMenuItem;
import org.scilab.modules.metanet.Metanet;
import org.scilab.modules.metanet.configuration.ConfigurationManager;
import org.scilab.modules.metanet.configuration.model.DocumentType;
import org.scilab.modules.metanet.configuration.utils.ConfigurationConstants;
import org.scilab.modules.metanet.graph.MetanetDiagram;

/**
 * Implement the recent file actions.
 * 
 * This class doesn't follow standard action implementation.
 */
public final class RecentFileAction extends DefaultAction implements PropertyChangeListener {
    /** Name of the action */
    public static final String NAME = "";
    /** Icon name of the action */
    public static final String SMALL_ICON = "";
    /** Mnemonic key of the action */
    public static final int MNEMONIC_KEY = 0;
    /** Accelerator key for the action */
    public static final int ACCELERATOR_KEY = 0;

    private static final Map<URL, RecentFileAction> INSTANCE_REGISTRY = new HashMap<URL, RecentFileAction>();

    private File recentFile;
    private MenuItem menu;

    /**
     * @param file
     *            new recent file
     */
    private RecentFileAction(File file) {
        super(null);
        recentFile = file;
    }

    /**
     * @param menu
     *            the menu to set
     */
    private void setMenu(MenuItem menu) {
        this.menu = menu;
    }

    /**
     * @param e
     *            parameter
     * @see org.scilab.modules.graph.actions.base.DefaultAction#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        ConfigurationManager.getInstance().addToRecentFiles(recentFile.getPath());
        if (getGraph(null) == null) { // Called from palettes
            Metanet.openFromFile(recentFile.getPath());
        } else {
            ((MetanetDiagram) getGraph(null)).openDiagramFromFile(recentFile.getPath());
        }
        ConfigurationManager.getInstance().saveConfig();
    };

    /**
     * @param file
     *            new recent file
     * @return menu item
     */
    public static MenuItem createMenu(URL file) {
        File f;
        try {
            f = new File(file.toURI());
        } catch (URISyntaxException e) {
            Logger.getLogger(RecentFileAction.class.getName()).severe(e.toString());
            return null;
        }

        RecentFileAction action = INSTANCE_REGISTRY.get(file);
        if (action == null) {
            action = new RecentFileAction(f);
        }

        ConfigurationManager manager = ConfigurationManager.getInstance();
        manager.addPropertyChangeListener(ConfigurationConstants.RECENT_FILES_CHANGED, action);

        MenuItem m = ScilabMenuItem.createMenuItem();
        m.setCallback(action);
        m.setText(f.getName());

        action.setMenu(m);
        return m;
    }

    /**
     * Update the file association when it has been moved on the configuration
     * 
     * @param evt
     *            the event data
     * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        assert evt.getPropertyName().equals(ConfigurationConstants.RECENT_FILES_CHANGED);

        /*
         * This is the case where a new menu will be created. That's not our job
         * there.
         */
        if (evt.getOldValue() == null) {
            return;
        }

        /*
         * Create URL instance;
         */
        final String oldURL = ((DocumentType) evt.getOldValue()).getUrl();
        final String newURL = ((DocumentType) evt.getNewValue()).getUrl();

        URL old;
        URL current;
        try {
            old = new URL(oldURL);
            current = recentFile.toURI().toURL();
        } catch (MalformedURLException e) {
            Logger.getLogger(RecentFileAction.class.getName()).severe(e.toString());
            return;
        }

        /*
         * Return when it is not our associated file.
         */
        if (!current.sameFile(old)) {
            return;
        }

        /*
         * Our job then
         */
        try {
            URL newUrl = new URL(newURL);

            recentFile = new File(newUrl.toURI());
            menu.setText(recentFile.getName());

            INSTANCE_REGISTRY.remove(old);
            INSTANCE_REGISTRY.put(newUrl, this);

        } catch (URISyntaxException e) {
            Logger.getLogger(RecentFileAction.class.getName()).severe(e.toString());
        } catch (MalformedURLException e) {
            Logger.getLogger(RecentFileAction.class.getName()).severe(e.toString());
        }
    }
}
