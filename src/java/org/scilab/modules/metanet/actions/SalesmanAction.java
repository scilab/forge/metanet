/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import org.scilab.modules.action_binding.highlevel.ScilabInterpreterManagement;
import org.scilab.modules.action_binding.highlevel.ScilabInterpreterManagement.InterpreterException;
import org.scilab.modules.graph.ScilabGraph;
import org.scilab.modules.graph.actions.base.DefaultAction;
import org.scilab.modules.gui.menuitem.MenuItem;
import org.scilab.modules.javasci.JavasciException;
import org.scilab.modules.javasci.Scilab;
import org.scilab.modules.metanet.edge.BasicEdge;
import org.scilab.modules.metanet.graph.MetanetDiagram;
import org.scilab.modules.metanet.io.GraphWriter;
import org.scilab.modules.metanet.utils.MetanetMessages;
import org.scilab.modules.types.ScilabString;
import org.scilab.modules.types.ScilabType;

import com.mxgraph.model.mxGraphModel;

public class SalesmanAction extends DefaultAction {
    private static final long serialVersionUID = 1L;

    public static final String NAME = MetanetMessages.SALESMAN;
    public static final String SMALL_ICON = "";
    public static final int MNEMONIC_KEY = 0;
    public static final int ACCELERATOR_KEY = 0;

    private MetanetDiagram diagram;

    /**
     * Constructor
     * 
     * @param scilabGraph
     *            associated Scilab Graph
     */
    public SalesmanAction(ScilabGraph scilabGraph) {
        super(scilabGraph);
    }

    /**
     * Create a menu to add in Scilab Graph menu bar
     * 
     * @param scilabGraph
     *            associated Scilab Graph
     * @return the menu
     */
    public static MenuItem createMenu(ScilabGraph scilabGraph) {
        return createMenu(scilabGraph, SalesmanAction.class);
    }

    /**
     * Create a button to add in Scilab Graph tool bar
     * 
     * @param scilabGraph
     *            associated Scilab Graph
     * @return the button
     */
    public static JButton createButton(ScilabGraph scilabGraph) {
        return createButton(scilabGraph, SalesmanAction.class);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        diagram = ((MetanetDiagram) getGraph(e));
        if (!diagram.isDirected()) {
            return;
        }
        updateUI(true);

        GraphWriter.writeDiagramToScilab(diagram);

        final String command = "edgeIds = edges_2_ids(salesman(g),g);";
        try {
            ScilabInterpreterManagement.asynchronousScilabExec(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent arg0) {
                    ScilabType data;
                    try {
                        data = Scilab.getInCurrentScilabSession("edgeIds");
                    } catch (JavasciException e) {
                        updateUI(false);
                        return;
                    }

                    // if scilab doesn't return an empty matrix
                    if (data instanceof ScilabString) {
                        final ScilabString dataPath = (ScilabString) data;

                        String[] path = dataPath.getData()[0];
                        BasicEdge[] pathEdges = new BasicEdge[path.length];

                        for (int i = 0; i < path.length; ++i) {
                            pathEdges[i] = (BasicEdge) ((mxGraphModel) diagram.getModel()).getCell(path[i]);
                        }

                        diagram.setSelectionCells(pathEdges);
                        diagram.refresh();
                    }

                    updateUI(false);
                }
            }, command);
        } catch (InterpreterException e1) {
            e1.printStackTrace();
            updateUI(false);
        }
    }

    /**
     * Update the UI depending on the action selected or not
     * 
     * @param started
     *            the started status
     */
    public void updateUI(boolean started) {

        ((MetanetDiagram) getGraph(null)).setReadOnly(started);

        if (started) {
            ((MetanetDiagram) getGraph(null)).info("searching for a path...");
        } else {
            ((MetanetDiagram) getGraph(null)).info(MetanetMessages.EMPTY_INFO);
        }
    }

}
