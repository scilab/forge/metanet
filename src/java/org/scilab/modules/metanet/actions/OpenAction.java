/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.actions;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JFileChooser;

import org.scilab.modules.action_binding.highlevel.ScilabInterpreterManagement;
import org.scilab.modules.graph.ScilabGraph;
import org.scilab.modules.graph.actions.base.DefaultAction;
import org.scilab.modules.gui.bridge.filechooser.SwingScilabFileChooser;
import org.scilab.modules.gui.filechooser.ScilabFileChooser;
import org.scilab.modules.gui.menuitem.MenuItem;
import org.scilab.modules.gui.utils.SciFileFilter;
import org.scilab.modules.metanet.utils.MetanetMessages;

public class OpenAction extends DefaultAction {

    private static final long serialVersionUID = 1L;

    public static final String NAME = MetanetMessages.OPEN;
    public static final String SMALL_ICON = "document-open";
    public static final int MNEMONIC_KEY = KeyEvent.VK_O;
    public static final int ACCELERATOR_KEY = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();

    /**
     * Constructor
     * 
     * @param scilabGraph
     *            associated Scilab Graph
     */
    public OpenAction(ScilabGraph scilabGraph) {
        super(scilabGraph);
    }

    /**
     * Create a menu to add in Scilab Graph menu bar
     * 
     * @param scilabGraph
     *            associated Scilab Graph
     * @return the menu
     */
    public static MenuItem createMenu(ScilabGraph scilabGraph) {
        return createMenu(scilabGraph, OpenAction.class);
    }

    /**
     * Create a button to add in Scilab Graph tool bar
     * 
     * @param scilabGraph
     *            associated Scilab Graph
     * @return the button
     */
    public static JButton createButton(ScilabGraph scilabGraph) {
        return createButton(scilabGraph, OpenAction.class);
    }

    /**
     * Open file action
     * 
     * @see org.scilab.modules.graph.actions.DefaultAction#doAction()
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        SwingScilabFileChooser fc = ((SwingScilabFileChooser) ScilabFileChooser.createFileChooser().getAsSimpleFileChooser());

        /* Standard files */
        fc.setTitle(MetanetMessages.OPEN);
        fc.setUiDialogType(JFileChooser.OPEN_DIALOG);
        fc.setMultipleSelection(false);

        /*
         * FIXME: why hardcoded values ?
         */
        SciFileFilter graphFilter = new SciFileFilter("*.graph", null, 0);
        SciFileFilter xgraphFilter = new SciFileFilter("*.xgraph", null, 0);
        SciFileFilter allFilter = new SciFileFilter("*.*", null, 2);

        fc.addChoosableFileFilter(xgraphFilter);
        fc.addChoosableFileFilter(graphFilter);
        fc.addChoosableFileFilter(allFilter);
        fc.setFileFilter(xgraphFilter);

        fc.setAcceptAllFileFilterUsed(false);
        fc.displayAndWait();

        if (fc.getSelection() == null || fc.getSelection().length == 0 || fc.getSelection()[0].equals("")) {
            return;
        }

        ScilabInterpreterManagement.requestScilabExec("edit_graph('" + fc.getSelection()[0] + "');");
    }
}