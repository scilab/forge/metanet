/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet;

import java.awt.GraphicsEnvironment;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;

import org.scilab.modules.graph.utils.ScilabExported;
import org.scilab.modules.gui.bridge.tab.SwingScilabDockablePanel;
import org.scilab.modules.gui.utils.ClosingOperationsManager;
import org.scilab.modules.localization.Messages;
import org.scilab.modules.metanet.configuration.ConfigurationManager;
import org.scilab.modules.metanet.graph.MetanetDiagram;
import org.scilab.modules.metanet.io.GraphReader;
import org.scilab.modules.metanet.io.GraphWriter;

public final class Metanet {
    private static final String IS_HEADLESS = Messages.gettext("a graphical environnement is needed.");

    /**
     * Create a diagram model with a visible window
     * 
     * @return The new empty model
     */
    public static MetanetDiagram createEmptyDiagram() {
        MetanetDiagram graph = createANotShownDiagram();

        MetanetTab.restore(graph);
        return graph;
    }

    /**
     * Create a diagram model
     * 
     * @return The new empty model
     */

    public static MetanetDiagram createANotShownDiagram() {
        MetanetDiagram graph = new MetanetDiagram();
        graph.installListeners();
        return graph;
    }

    /**
     * @return All the opened (hidden or shown) diagrams
     */
    public static List<MetanetDiagram> getDiagrams() {
        return MetanetTab.getAllDiagrams();
    }

    /**
     * Load the file into metanet then export it to 'g' as a Scilab variable
     * 
     * @param filename
     *            the filename to load
     */
    @ScilabExported(module = "metanet", filename = "Metanet.giws.xml")
    public static void loadIntoScilab(final String filename) {
        final MetanetDiagram graph = new MetanetDiagram();
        graph.transformAndLoadFile(new File(filename), true);
        GraphWriter.writeDiagramToScilab(graph);
    }

    /**
     * Entry point with filename
     * 
     * @param varname
     *            variable name to load
     * @param filename
     *            file name to load
     * @param window
     *            the window index to load into
     * @param scale
     *            the scale to set on the window
     * @param winsize
     *            the window size to set
     * @throws InterruptedException
     * @throws InvocationTargetException
     */
    @ScilabExported(module = "metanet", filename = "Metanet.giws.xml")
    public static void metanet(final String varname, final String filename, final double window, final double scale, final double[] winsize)
            throws InterruptedException, InvocationTargetException {
        /* Check not headless */
        if (GraphicsEnvironment.isHeadless()) {
            throw new RuntimeException(IS_HEADLESS);
        }

        SwingUtilities.invokeAndWait(new Runnable() {
            @Override
            public void run() {
                if (varname == null && filename == null) {
                    createEmptyDiagram();
                } else {
                    if (varname != null) {
                        openFromVariable(varname);
                    }
                    if (filename != null) {
                        openFromFile(filename);
                    }
                }
            }
        });
    }

    public static void openFromFile(final String filename) {
        ConfigurationManager.getInstance().addToRecentFiles(filename);
        if (!MetanetTab.focusOnExistingFile(filename)) {
            MetanetDiagram diagram = createEmptyDiagram();
            diagram.openDiagramFromFile(filename);
            diagram.refresh();
        }
        ConfigurationManager.getInstance().saveConfig();
    }

    public static void openFromVariable(final String variable) {
        MetanetDiagram diagram = createEmptyDiagram();
        diagram.openDiagram(GraphReader.readDiagramFromScilab(variable));
        diagram.refresh();
    }

    /**
     * Close the current metanet session
     */
    public static void closeSession() {
        List<MetanetDiagram> diagrams = MetanetTab.getAllDiagrams();

        final ArrayList<SwingScilabDockablePanel> tabs = new ArrayList<SwingScilabDockablePanel>();
        for (MetanetDiagram graph : diagrams) {
            SwingScilabDockablePanel tab = MetanetTab.get(graph);
            if (tab != null) {
                tabs.add(tab);
            }

            tab = ViewPortTab.get(graph);
            if (tab != null) {
                tabs.add(tab);
            }
        }

        ClosingOperationsManager.startClosingOperation(tabs, true, true);
    }

    public static void warnObjectByUID(String uid, String message) {
        /* Check not headless */
        if (GraphicsEnvironment.isHeadless()) {
            throw new RuntimeException(IS_HEADLESS);
        }

        // Try to find a block with given index (UID)
        List<MetanetDiagram> allDiagrams = Metanet.getDiagrams();
        for (int i = 0; i < allDiagrams.size(); ++i) {
            allDiagrams.get(i).warnObjectByUID(uid, message);
        }
    }

    /*
     * netwindow API
     */

    /**
     * @see help netwindow
     * @param index
     *            current index to set (if negative, do not update anything)
     * @return previous current index (maybe negative if nothing has focus)
     */
    @ScilabExported(filename = "Metanet.giws.xml", module = "metanet")
    public static double netwindow(final double index) {
        /* Check not headless */
        if (GraphicsEnvironment.isHeadless()) {
            throw new RuntimeException(IS_HEADLESS);
        }

        final int previous = MetanetTab.getCurrentDiagram();
        MetanetTab.setCurrent((int) index);

        return previous;
    }

    /**
     * @see help netwindows
     * @return list of available windows
     */
    @ScilabExported(filename = "Metanet.giws.xml", module = "metanet")
    public static double[] netwindows() {
        /* Check not headless */
        if (GraphicsEnvironment.isHeadless()) {
            throw new RuntimeException(IS_HEADLESS);
        }

        final int len = MetanetTab.getAllDiagrams().size();

        double[] ret = new double[len];
        for (int i = 0; i < len; i++) {
            ret[i] = i + 1;
        }

        return ret;
    }

    /**
     * @see help show_edges
     * @see help show_nodes
     */
    @ScilabExported(filename = "Metanet.giws.xml", module = "metanet")
    public static void show(final boolean nodes, final boolean edges, final double[] indexes, final boolean clearPrevious, final String message) {
        /* Check not headless */
        if (GraphicsEnvironment.isHeadless()) {
            throw new RuntimeException(IS_HEADLESS);
        }

        final int index = MetanetTab.getCurrentDiagram();

        if (index > 0) {
            final MetanetDiagram graph = getDiagrams().get(index - 1);
            graph.show(nodes, edges, indexes, clearPrevious, message);
        }
    }

    /**
     * This method is called when the user exits from Scilab
     */
    public static void closeMetanetFromScilab() {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final List<SwingScilabDockablePanel> tabs = new ArrayList<SwingScilabDockablePanel>();
                for (MetanetDiagram d : MetanetTab.getAllDiagrams()) {
                    SwingScilabDockablePanel t = MetanetTab.get(d);
                    if (t != null) {
                        tabs.add(t);
                    }

                    t = ViewPortTab.get(d);
                    if (t != null) {
                        tabs.add(t);
                    }
                }

                ClosingOperationsManager.startClosingOperation(tabs, true, true);
            }
        });
    }

}
