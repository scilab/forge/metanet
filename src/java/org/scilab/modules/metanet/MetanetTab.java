/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;
import java.util.Vector;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.Timer;

import org.scilab.modules.graph.actions.CopyAction;
import org.scilab.modules.graph.actions.CutAction;
import org.scilab.modules.graph.actions.DeleteAction;
import org.scilab.modules.graph.actions.InvertSelectionAction;
import org.scilab.modules.graph.actions.PasteAction;
import org.scilab.modules.graph.actions.RedoAction;
import org.scilab.modules.graph.actions.SelectAllAction;
import org.scilab.modules.graph.actions.UndoAction;
import org.scilab.modules.graph.actions.ZoomInAction;
import org.scilab.modules.graph.actions.ZoomOutAction;
import org.scilab.modules.gui.bridge.tab.SwingScilabDockablePanel;
import org.scilab.modules.gui.bridge.toolbar.SwingScilabToolBar;
import org.scilab.modules.gui.bridge.window.SwingScilabDockingWindow;
import org.scilab.modules.gui.bridge.window.SwingScilabWindow;
import org.scilab.modules.gui.menu.Menu;
import org.scilab.modules.gui.menu.ScilabMenu;
import org.scilab.modules.gui.menubar.MenuBar;
import org.scilab.modules.gui.menubar.ScilabMenuBar;
import org.scilab.modules.gui.messagebox.ScilabModalDialog;
import org.scilab.modules.gui.messagebox.ScilabModalDialog.AnswerOption;
import org.scilab.modules.gui.messagebox.ScilabModalDialog.ButtonType;
import org.scilab.modules.gui.messagebox.ScilabModalDialog.IconType;
import org.scilab.modules.gui.tabfactory.ScilabTabFactory;
import org.scilab.modules.gui.textbox.ScilabTextBox;
import org.scilab.modules.gui.toolbar.ScilabToolBar;
import org.scilab.modules.gui.toolbar.ToolBar;
import org.scilab.modules.gui.utils.BarUpdater;
import org.scilab.modules.gui.utils.ClosingOperationsManager;
import org.scilab.modules.gui.utils.WindowsConfigurationManager;
import org.scilab.modules.metanet.actions.CircuitAction;
import org.scilab.modules.metanet.actions.CloseAction;
import org.scilab.modules.metanet.actions.FitDiagramToViewAction;
import org.scilab.modules.metanet.actions.NewDiagramAction;
import org.scilab.modules.metanet.actions.OpenAction;
import org.scilab.modules.metanet.actions.QuitAction;
import org.scilab.modules.metanet.actions.RecentFileAction;
import org.scilab.modules.metanet.actions.SalesmanAction;
import org.scilab.modules.metanet.actions.SaveAction;
import org.scilab.modules.metanet.actions.SaveAsAction;
import org.scilab.modules.metanet.actions.ShortestPathAction;
import org.scilab.modules.metanet.actions.ViewLabelsAction;
import org.scilab.modules.metanet.configuration.ConfigurationManager;
import org.scilab.modules.metanet.configuration.model.DocumentType;
import org.scilab.modules.metanet.configuration.utils.ConfigurationConstants;
import org.scilab.modules.metanet.edge.BasicEdge;
import org.scilab.modules.metanet.edge.actions.AddEdgeDataFieldAction;
import org.scilab.modules.metanet.edge.actions.EditDefaultEdgeAction;
import org.scilab.modules.metanet.graph.MetanetDiagram;
import org.scilab.modules.metanet.node.BasicNode;
import org.scilab.modules.metanet.node.actions.AddNodeDataFieldAction;
import org.scilab.modules.metanet.node.actions.EditDefaultNodeAction;
import org.scilab.modules.metanet.utils.MetanetMessages;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;

public class MetanetTab extends SwingScilabDockablePanel {
    private static final long serialVersionUID = 1L;

    public static final String DEFAULT_WIN_UUID = "metanet-default-window";
    public static final String DEFAULT_TAB_UUID = "metanet-default-tab";

    /*
     * Static fields
     */
    private static List<MetanetDiagram> diagrams = new Vector<MetanetDiagram>();
    private static int currentDiagram = 0;

    /*
     * Instance fields
     */
    private MenuBar menuBar;
    private Menu fileMenu;
    private Menu recentsMenu;
    private Menu graphMenu;
    private Menu edit;
    private Menu compute;

    private JCheckBoxMenuItem viewport;

    private JButton openAction;
    private JButton saveAction;
    private JButton saveAsAction;
    private JButton newDiagramAction;
    private JButton deleteAction;
    private JButton undoAction;
    private JButton redoAction;
    private JButton fitDiagramToViewAction;
    private JButton zoomInAction;
    private JButton zoomOutAction;

    private static class ClosingOperation implements org.scilab.modules.gui.utils.ClosingOperationsManager.ClosingOperation {
        private final MetanetDiagram graph;

        public ClosingOperation(MetanetDiagram graph) {
            this.graph = graph;
        }

        @Override
        public int canClose() {
            boolean canClose = !graph.isModified();

            if (!canClose) {
                final AnswerOption answer = ScilabModalDialog.show(MetanetTab.get(graph), MetanetMessages.DIAGRAM_MODIFIED, MetanetMessages.METANET,
                        IconType.QUESTION_ICON, ButtonType.YES_NO_CANCEL);

                switch (answer) {
                case YES_OPTION:
                    canClose = graph.saveDiagram();
                    break;
                case NO_OPTION:
                    canClose = true;
                    break;
                default:
                    canClose = false;
                    break;
                }
            }

            /*
             * Update configuration before the destroy call to validate the uuid
             */
            if (canClose) {
                ConfigurationManager.getInstance().updateOpenedTab(graph);
                ConfigurationManager.getInstance().saveConfig();
            }

            return canClose ? 1 : 0;
        }

        @Override
        public void destroy() {
            // if is current diagram, the clear the current
            if (getCurrentDiagram() == diagrams.indexOf(graph) + 1) {
                setCurrent(0);
            }

            diagrams.remove(graph);
        }

        @Override
        public String askForClosing(final List<SwingScilabDockablePanel> list) {
            final ArrayList<SwingScilabDockablePanel> tabs = new ArrayList<SwingScilabDockablePanel>();
            for (MetanetDiagram graph : diagrams) {
                SwingScilabDockablePanel tab = get(graph);
                if (tab != null) {
                    tabs.add(tab);
                }

                tab = ViewPortTab.get(graph);
                if (tab != null) {
                    tabs.add(tab);
                }
            }

            if (list.containsAll(tabs)) {
                return MetanetMessages.METANET;
            } else {
                return null;
            }
        }

        @Override
        public void updateDependencies(List<SwingScilabDockablePanel> list, ListIterator<SwingScilabDockablePanel> it) {
        }
    }

    private static class EndedRestoration implements WindowsConfigurationManager.EndedRestoration {
        private final MetanetDiagram graph;

        public EndedRestoration(MetanetDiagram graph) {
            this.graph = graph;
        }

        @Override
        public void finish() {
            graph.updateTabTitle();

            ConfigurationManager.getInstance().removeOpenedTab(graph);
        }
    }

    /**
     * Default constructor
     * 
     * @param diagram
     *            The associated diagram model
     */
    public MetanetTab(MetanetDiagram diagram, String uuid) {
        super(MetanetMessages.METANET, uuid);

        setAssociatedXMLIDForHelp("metanet");

        diagram.setGraphTab(uuid);
        // setWindowIcon(Xcos.ICON.getImage());

        initComponents(diagram);

        diagram.getAsComponent().addKeyListener(new ArrowKeysListener());
    }

    /*
     * Static API for Tabs
     */

    /**
     * Get the tab for a graph.
     * 
     * @param graph
     *            the graph
     * @return the tab (can be null)
     */
    public static MetanetTab get(MetanetDiagram graph) {
        final String uuid = graph.getGraphTab();
        if (uuid == null) {
            return null;
        }

        return (MetanetTab) ScilabTabFactory.getInstance().getFromCache(uuid);
    }

    /**
     * Restore or create the viewport tab for the graph
     * 
     * @param graph
     *            the graph
     */
    public static void restore(MetanetDiagram graph) {
        restore(graph, true);
    }

    /**
     * Restore or create the tab for the graph
     * 
     * @param graph
     *            the graph
     * @param visible
     *            should the tab should be visible
     */
    public static void restore(final MetanetDiagram graph, final boolean visible) {
        String uuid = graph.getGraphTab();
        if (uuid == null) {
            uuid = UUID.randomUUID().toString();
        }

        final MetanetTab tab = new MetanetTab(graph, uuid);
        ScilabTabFactory.getInstance().addToCache(tab);

        diagrams.add(graph);
        setCurrent(graph);
        graph.setOpened(true);

        if (visible) {
            tab.createDefaultWindow().setVisible(true);

            graph.updateTabTitle();
            BarUpdater.updateBars(tab.getParentWindowId(), tab.getMenuBar(), tab.getToolBar(), tab.getInfoBar(), tab.getName(), tab.getWindowIcon());
        }

        ClosingOperationsManager.addDependencyWithRoot(tab);
        ClosingOperationsManager.registerClosingOperation(tab, new ClosingOperation(graph));
        WindowsConfigurationManager.registerEndedRestoration(tab, new EndedRestoration(graph));
    }

    /*
     * Static API for current window management
     */

    /**
     * Set the current graph, this method is linked to Scilab netwindow
     * 
     * @param graph
     *            the graph to set as current
     */
    protected static void setCurrent(final MetanetDiagram graph) {
        currentDiagram = diagrams.indexOf(graph) + 1;
    }

    /**
     * Set the current graph tab, this method is linked to Scilab netwindow
     * 
     * @param index
     *            the index to set (nothing is performed if negative or greater
     *            than diagrams)
     */
    protected static void setCurrent(final int index) {
        final int len = getAllDiagrams().size();

        if (index >= 0 && index <= len) {
            currentDiagram = index;
        }
    }

    /**
     * Retrieve the current Scilab netwindow index
     * 
     * @return the netwindow index
     */
    public static int getCurrentDiagram() {
        return currentDiagram;
    }

    /**
     * Instantiate all the subcomponents of this Tab.
     */
    private void initComponents(MetanetDiagram diagram) {
        /* Create the menu bar */
        menuBar = createMenuBar(diagram);
        setMenuBar(menuBar);

        /* Create the toolbar */
        final ToolBar toolBar = createToolBar(diagram);
        setToolBar(toolBar);

        // No SimpleTab.addMember(ScilabComponent ...) so perform a raw
        // association.
        setContentPane(diagram.getAsComponent());

        /* Create the infoBar */
        setInfoBar(ScilabTextBox.createTextBox());
    }

    /**
     * Move cells with the arrow keys.
     */
    private class ArrowKeysListener implements KeyListener {

        private static final double DEFAULT_PIXEL_MOVE = 1;
        private static final double MODIFIER_FACTOR = 5;
        private static final int DEFAULT_DELAY = 800; // milliseconds

        private double xIncrement;
        private double yIncrement;
        private mxGraph graph;

        private final Timer repetitionTimer;
        private final ActionListener doMove = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                if (graph != null) {
                    Object[] cells = graph.getSelectionCells();

                    graph.getModel().beginUpdate();
                    for (Object cell : cells) {
                        if (cell instanceof BasicEdge || cell instanceof BasicNode) {
                            graph.translateCell(cell, xIncrement, yIncrement);
                        }
                    }
                    graph.getModel().endUpdate();
                    graph.refresh();
                }
            }
        };

        /**
         * Constructor
         */
        public ArrowKeysListener() {
            repetitionTimer = new Timer(DEFAULT_DELAY, doMove);
            repetitionTimer.setInitialDelay(0);
        }

        /**
         * Get the action parameters and start the action timer.
         * 
         * @param e
         *            key event
         */
        @Override
        public void keyPressed(KeyEvent e) {
            double realMove;
            boolean mustMove = true;

            mxGraphComponent sourceDiagram = (mxGraphComponent) e.getSource();
            graph = sourceDiagram.getGraph();

            if (graph.isGridEnabled()) {
                realMove = graph.getGridSize();
            } else {
                realMove = DEFAULT_PIXEL_MOVE;
            }

            if (e.getModifiers() == Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()) {
                realMove *= MODIFIER_FACTOR;
            }

            switch (e.getKeyCode()) {
            case KeyEvent.VK_UP:
                yIncrement = -realMove;
                break;

            case KeyEvent.VK_DOWN:
                yIncrement = realMove;
                break;

            case KeyEvent.VK_RIGHT:
                xIncrement = realMove;
                break;

            case KeyEvent.VK_LEFT:
                xIncrement = -realMove;
                break;

            default:
                mustMove = false;
                break;
            }

            if (!mustMove) {
                return;
            }

            if (!graph.isGridEnabled()) {
                xIncrement *= sourceDiagram.getZoomFactor();
                yIncrement *= sourceDiagram.getZoomFactor();
            }

            repetitionTimer.start();
        }

        /**
         * Stop the action timer and clear parameters
         * 
         * @param e
         *            key event
         */
        @Override
        public void keyReleased(KeyEvent e) {
            repetitionTimer.stop();
            yIncrement = 0;
            xIncrement = 0;
            graph = null;
        }

        /**
         * Not used there
         * 
         * @param e
         *            Not used
         */
        @Override
        public void keyTyped(KeyEvent e) {
        }
    }

    /**
     * Try to focus to an already openned file
     * 
     * @param filename
     *            filename
     * @return True when found and focused, False otherwise
     */
    public static boolean focusOnExistingFile(String filename) {
        for (MetanetDiagram diagram : diagrams) {
            if (diagram.getSavedFile() != null) {
                if (diagram.getSavedFile().compareTo(new File(filename)) == 0) {
                    final MetanetTab tab = get(diagram);
                    if (tab != null) {
                        tab.setCurrent();
                    }

                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Create the windows menu bar
     * 
     * @param diagram
     *            graph the associated graph
     * @return menu bar
     */
    private MenuBar createMenuBar(final MetanetDiagram diagram) {

        menuBar = ScilabMenuBar.createMenuBar();

        /** FILE MENU */
        fileMenu = ScilabMenu.createMenu();
        fileMenu.setText(MetanetMessages.FILE);
        fileMenu.setMnemonic('F');

        fileMenu.add(NewDiagramAction.createMenu(diagram));

        fileMenu.add(OpenAction.createMenu(diagram));

        recentsMenu = ScilabMenu.createMenu();
        recentsMenu.setText(MetanetMessages.RECENT_FILES);

        final ConfigurationManager manager = ConfigurationManager.getInstance();
        final List<DocumentType> recentFiles = manager.getSettings().getRecentFiles().getDocument();
        for (int i = 0; i < recentFiles.size(); i++) {
            URL url;
            try {
                url = new URL(recentFiles.get(i).getUrl());
            } catch (MalformedURLException e) {
                Logger.getLogger(MetanetTab.class.getName()).severe(e.toString());
                break;
            }
            recentsMenu.add(RecentFileAction.createMenu(url));
        }

        ConfigurationManager.getInstance().addPropertyChangeListener(ConfigurationConstants.RECENT_FILES_CHANGED, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                assert evt.getPropertyName().equals(ConfigurationConstants.RECENT_FILES_CHANGED);

                /*
                 * We only handle menu creation there. Return when this is not
                 * the case.
                 */
                if (evt.getOldValue() != null) {
                    return;
                }

                URL url;
                try {
                    url = new URL(((DocumentType) evt.getNewValue()).getUrl());
                } catch (MalformedURLException e) {
                    Logger.getLogger(MetanetTab.class.getName()).severe(e.toString());
                    return;
                }

                recentsMenu.add(RecentFileAction.createMenu(url));
            }
        });

        fileMenu.add(recentsMenu);

        fileMenu.addSeparator();
        fileMenu.add(SaveAction.createMenu(diagram));
        fileMenu.add(SaveAsAction.createMenu(diagram));
        // fileMenu.add(ExportAction.createMenu(scilabGraph));
        fileMenu.add(CloseAction.createMenu(diagram));
        fileMenu.addSeparator();
        fileMenu.add(QuitAction.createMenu(diagram));

        menuBar.add(fileMenu);

        /** Graph menu */

        graphMenu = ScilabMenu.createMenu();
        graphMenu.setText(MetanetMessages.GRAPH);
        graphMenu.setMnemonic('G');

        graphMenu.add(EditDefaultNodeAction.createMenu(diagram));
        graphMenu.add(EditDefaultEdgeAction.createMenu(diagram));
        graphMenu.add(AddEdgeDataFieldAction.createMenu(diagram));
        graphMenu.add(AddNodeDataFieldAction.createMenu(diagram));
        graphMenu.add(ViewLabelsAction.createMenu(diagram));

        menuBar.add(graphMenu);

        /** Edit menu */
        edit = ScilabMenu.createMenu();
        edit.setText(MetanetMessages.EDIT);
        edit.setMnemonic('E');
        menuBar.add(edit);

        edit.add(UndoAction.undoMenu(diagram));
        edit.add(RedoAction.redoMenu(diagram));
        edit.addSeparator();
        edit.add(CutAction.cutMenu(diagram));
        edit.add(CopyAction.copyMenu(diagram));
        edit.add(PasteAction.pasteMenu(diagram));
        edit.add(DeleteAction.createMenu(diagram));
        edit.addSeparator();
        edit.add(SelectAllAction.createMenu(diagram));
        edit.add(InvertSelectionAction.createMenu(diagram));
        edit.addSeparator();
        // edit.add(BlockParametersAction.createMenu(diagram));

        menuBar.add(edit);

        /** Compute Menu */
        compute = ScilabMenu.createMenu();
        compute.setText(MetanetMessages.COMPUTE);
        compute.setMnemonic('C');
        menuBar.add(compute);

        compute.add(ShortestPathAction.createMenu(diagram));
        compute.add(CircuitAction.createMenu(diagram));
        compute.add(SalesmanAction.createMenu(diagram));

        return menuBar;

    }

    /**
     * @return tool bar
     */
    private ToolBar createToolBar(final MetanetDiagram diagram) {
        ToolBar toolBar = ScilabToolBar.createToolBar();
        final SwingScilabToolBar stb = (SwingScilabToolBar) toolBar.getAsSimpleToolBar();

        newDiagramAction = NewDiagramAction.createButton(diagram);
        stb.add(newDiagramAction);

        openAction = OpenAction.createButton(diagram);
        stb.add(openAction);

        stb.addSeparator();

        saveAction = SaveAction.createButton(diagram);
        stb.add(saveAction);
        saveAsAction = SaveAsAction.createButton(diagram);
        stb.add(saveAsAction);

        stb.addSeparator();

        /*
         * printAction = PrintAction.createButton(diagram);
         * stb.add(printAction);
         */

        stb.addSeparator();

        deleteAction = DeleteAction.createButton(diagram);
        stb.add(deleteAction);

        stb.addSeparator();

        // UNDO / REDO
        undoAction = UndoAction.undoButton(diagram);
        redoAction = RedoAction.redoButton(diagram);
        stb.add(undoAction);
        stb.add(redoAction);

        stb.addSeparator();

        fitDiagramToViewAction = FitDiagramToViewAction.createButton(diagram);
        stb.add(fitDiagramToViewAction);

        stb.addSeparator();

        // ZOOMIN / ZOOMOUT
        zoomInAction = ZoomInAction.zoominButton(diagram);
        stb.add(zoomInAction);
        zoomOutAction = ZoomOutAction.zoomoutButton(diagram);
        stb.add(zoomOutAction);

        return toolBar;
    }

    /**
     * Check/uncheck the viewport check box menu item
     * 
     * @param status
     *            the checked status
     */
    public void setViewportChecked(boolean status) {
        viewport.setSelected(status);
    }

    /**
     * Get the check/uncheck status of the check box menu item
     * 
     * @param status
     *            the checked status
     */
    public boolean isViewportChecked() {
        return viewport.isSelected();
    }

    // /**
    // * @param xcosDiagramm
    // * diagram
    // */
    // public static void createViewPort(ScilabGraph metanetDiagramm) {
    // Window outline = ScilabWindow.createWindow();
    // Tab outlineTab = ScilabTab.createTab(MetanetMessages.VIEWPORT);
    //
    // outlineTab.setCallback(new CloseViewportAction(metanetDiagramm));
    //
    // MenuBar vpMenuBar = ScilabMenuBar.createMenuBar();
    // outlineTab.addMenuBar(vpMenuBar);
    //
    // Menu vpMenu = ScilabMenu.createMenu();
    // vpMenu.setText(MetanetMessages.VIEWPORT);
    // vpMenu.setMnemonic('V');
    // vpMenuBar.add(vpMenu);
    //
    // vpMenu.add(CloseViewportAction.createMenu(metanetDiagramm));
    //
    // outlineTab.getAsSimpleTab().setInfoBar(ScilabTextBox.createTextBox());
    //
    // ((MetanetDiagram) metanetDiagramm).setViewPort(outlineTab);
    //
    // // Creates the graph outline component
    // mxGraphOutline graphOutline = new
    // mxGraphOutline(metanetDiagramm.getAsComponent());
    //
    // graphOutline.setDrawLabels(true);
    //
    // outlineTab.setContentPane(graphOutline);
    // outline.addTab(outlineTab);
    // outline.setVisible(false);
    // outlineTab.setVisible(false);
    // }

    /**
     * @return diagram list
     */
    public static List<MetanetDiagram> getAllDiagrams() {
        return diagrams;
    }

    private SwingScilabWindow createDefaultWindow() {
        final SwingScilabWindow win;

        final SwingScilabWindow configuration = WindowsConfigurationManager.createWindow(DEFAULT_WIN_UUID, false);
        if (configuration != null) {
            win = configuration;
        } else {
            win = new SwingScilabDockingWindow();
        }

        win.addTab(this);
        return win;
    }
}
