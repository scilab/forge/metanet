/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2012 - Scilab Enterprises - Clement DAVID
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.graph.swing.handler;

import org.scilab.modules.metanet.graph.swing.GraphComponent;

import com.mxgraph.swing.handler.mxConnectionHandler;

/**
 * Connection handler used to update point dynamically
 */
public class ConnectionHandler extends mxConnectionHandler {

    public ConnectionHandler(GraphComponent graphComponent) {
        super(graphComponent);

        getMarker().setHotspot(0.1);
    }
}
