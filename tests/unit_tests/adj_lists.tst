// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) ????-2008 - INRIA - Serge STEER <serge.steer@inria.fr>
// Copyright (C) ????-2008 - INRIA - Claude GOMEZ <claude.gomez@inria.fr>
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================

// <-- TEST WITH GRAPHIC -->

// unit tests for adj_lists function
// =============================================================================

ta=[2 3 3 5 3 4 4 5 8];
he=[1 2 4 2 6 6 7 7 4];
g=make_graph('foo',1,8,ta,he);
g('node_x')=[129 200 283 281 128 366 122 333];
g('node_y')=[61 125 129 189 173 135 236 249];
show_graph(g);
[l_p,l_a,l_s]=adj_lists(g);
if l_p <> [ 1.    1.    2.    5.    7.    9.    9.    9.    10.] then pause,end
if l_a <> [ 1.    2.    3.    5.    6.    7.    4.    8.    9.] then pause,end
if l_s <> [ 1.    2.    4.    6.    6.    7.    2.    7.    4.] then pause,end

[l_p,l_a,l_s]=adj_lists(1,g('node_number'),ta,he);
if l_s <> [ 1.    2.    4.    6.    6.    7.    2.    7.    4.] then pause,end
if l_a <> [ 1.    2.    3.    5.    6.    7.    4.    8.    9.] then pause,end
if l_p <> [ 1.    1.    2.    5.    7.    9.    9.    9.    10.] then pause,end

