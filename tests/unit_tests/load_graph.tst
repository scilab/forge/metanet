// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) ????-2008 - INRIA - Serge STEER <serge.steer@inria.fr>
// Copyright (C) ????-2008 - INRIA - Claude GOMEZ <claude.gomez@inria.fr>
// Copyright (C) 2012 - Scilab Enterprises - Clement DAVID
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================

// unit tests for load_graph function
// =============================================================================

g=load_graph(metanet_module_path()+"/tests/unit_tests/mesh100.graph");
assert_checkequal(g.version, "5.0.1")
assert_checkequal(g.nodes.number, 100)

g=load_graph(metanet_module_path()+'/demos/colored.graph');
assert_checkequal(g.version, "5.0.1")
assert_checkequal(g.nodes.number, 10)

g=load_graph(metanet_module_path()+"/tests/unit_tests/mesh100.xgraph");
assert_checkequal(g.version, "5.0.1")
assert_checkequal(g.nodes.number, 100)

g=load_graph(metanet_module_path()+'/demos/colored.xgraph');
assert_checkequal(g.version, "5.0.1")
assert_checkequal(g.nodes.number, 10)

