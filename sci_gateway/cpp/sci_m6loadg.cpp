
/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "function.hxx"
#include "double.hxx"
#include "list.hxx"
#include "string.hxx"

extern "C"
{
#include "Scierror.h"
#include "localization.h"
#include "sci_malloc.h" // FREE
/* c -> fortran -> c woop */
void C2F(loadg)(
           char * path,                  int * lpath,
          char ** name,                  int * lname,
            int * directed,              int * n,
           int ** tail,                 int ** head,
         char *** node_name,            int ** node_type,
           int ** node_x,               int ** node_y,
           int ** node_color,           int ** node_diam,
           int ** node_border,          int ** node_font_size,
        double ** node_demand,        char *** edge_name,
           int ** edge_color,           int ** edge_width,
           int ** edge_hi_width,        int ** edge_font_size,
        double ** edge_length,       double ** edge_cost,
        double ** edge_min_cap,      double ** edge_max_cap,
        double ** edge_q_weight,     double ** edge_q_orig,
        double ** edge_weight,           int * default_node_diam,
            int * default_node_border,   int * default_edge_width,
            int * default_edge_hi_width, int * default_font_size,
            int * ndim,                  int * ma);
}

types::Function::ReturnValue
sci_loadg(
        typed_list & in,
        int iRetCount,
        typed_list & out)
{

    char * path            = NULL; int lpath                = 0;
    char * name            = NULL; int lname                = 0;
    int * directed         = NULL; int * n               = NULL;
    int * tail             = NULL; int * head            = NULL;
    char ** node_name      = NULL; int * node_type       = NULL;
    int * node_x           = NULL; int * node_y          = NULL;
    int * node_color       = NULL; int * node_diam       = NULL;
    int * node_border      = NULL; int * node_font_size  = NULL;
    double * node_demand   = NULL; char ** edge_name     = NULL;
    int * edge_color       = NULL; int * edge_width      = NULL;
    int * edge_hi_width    = NULL; int * edge_font_size  = NULL;
    double * edge_length   = NULL; double * edge_cost    = NULL;
    double * edge_min_cap  = NULL; double * edge_max_cap = NULL;
    double * edge_q_weight = NULL; double * edge_q_orig  = NULL;
    double * edge_weight   = NULL; int default_node_diam    = 0;
    int default_node_border   = 0; int default_edge_width   = 0;
    int default_edge_hi_width = 0; int default_font_size    = 0;
    int ndim                  = 0; int ma                   = 0;
    types::String * o_name                  = NULL;
    types::Double * o_lname                 = NULL;
    types::Double * o_directed              = NULL;
    types::Double * o_n                     = NULL;
    types::Double * o_tail                  = NULL;
    types::Double * o_head                  = NULL;
    types::String * o_node_name             = NULL;
    types::Double * o_node_type             = NULL;
    types::Double * o_node_x                = NULL;
    types::Double * o_node_y                = NULL;
    types::Double * o_node_color            = NULL;
    types::Double * o_node_diam             = NULL;
    types::Double * o_node_border           = NULL;
    types::Double * o_node_font_size        = NULL;
    types::Double * o_node_demand           = NULL;
    types::String * o_edge_name             = NULL;
    types::Double * o_edge_color            = NULL;
    types::Double * o_edge_width            = NULL;
    types::Double * o_edge_hi_width         = NULL;
    types::Double * o_edge_font_size        = NULL;
    types::Double * o_edge_length           = NULL;
    types::Double * o_edge_cost             = NULL;
    types::Double * o_edge_min_cap          = NULL;
    types::Double * o_edge_max_cap          = NULL;
    types::Double * o_edge_q_weight         = NULL;
    types::Double * o_edge_q_orig           = NULL;
    types::Double * o_edge_weight           = NULL;
    types::Double * o_default_node_diam     = NULL;
    types::Double * o_default_node_border   = NULL;
    types::Double * o_default_edge_width    = NULL;
    types::Double * o_default_edge_hi_width = NULL;
    types::Double * o_default_font_size     = NULL;

    char * fname = "loadg";

    if (in.size() != 1)
    {
        Scierror(999, _("%s: Invalid number of input arguments: "
                        "Expected %d, got %d instead.\n"),
                 fname, 1, in.size());
        return 1;
    }

    if (iRetCount != 1)
    {
        Scierror(999, _("%s: Invalid number of output arguments: "
                        "Expected %d, got %d instead.\n"),
                 fname, 1, iRetCount);
        return 1;
    }

    /* checking variable path (number 1) */
    if (in[0]->isString() == false)
    {
        Scierror(999, _("%s: Expected a string for "
                        "input argument %s at position #%d, "
                        "but got a %s instead.\n"),
                 fname, "path", 1, in[0]->getTypeStr());
        return 1;
    }
    path = in[0]->getAs<types::String>();
    if (path->getSize() != 1)
    {
        Scierror(999, _("%s: Expected a (1, 1) for "
                        "input argument %s at position #%d, "
                        "but got a (%d, %d) instead.\n"),
                 fname, "path", 1, path->getRows(), path->getCols());
        return 1;
    }
    path_pc = (char*)(path->get()); /* though it's really a wchar_t */
    path_l = 0; /* determine number of bytes */
    while (path_pc[path_l]) /* != '\0' */
        path_l++;

    /* no cross variable size checking ?! */

    /* prepare output variables */
    /* actually we just give direct content/pointers to
     * loadg and then put that in objects
     * (loadg does its own MALLOC ...) */

    /***********************/
    /*                     */
    /*    CALL TO LOADG    */
    /*                     */
    /***********************/
    C2F(loadg)(
            /* input */
            path,                   &lpath,
            /* output */
            &name,                  &lname,
            &directed,              &n,
            &tail,                  &head,
            &node_name,             &node_type,
            &node_x,                &node_y,
            &node_color,            &node_diam,
            &node_border,           &node_font_size,
            &node_demand,           &edge_name,
            &edge_color,            &edge_width,
            &edge_hi_width,         &edge_font_size,
            &edge_length,           &edge_cost,
            &edge_min_cap,          &edge_max_cap,
            &edge_q_weight,         &edge_q_orig,
            &edge_weight,           &default_node_diam,
            &default_node_border,   &default_edge_width,
            &default_edge_hi_width, &default_font_size,
            &ndim,                  &ma);

    /* error detection */
    /* no error detection ?! */

    /* pack output variables */
    graph = new types::List();
    /* name */
    o_name = new types::String(name);
    FREE(name);
    graph->append(o_name);
    /* lname */
    o_lname = new types::Double(1, 1);
    *(o_lname->get()) = static_cast<double>(lname);
    graph->append(o_lname);
    /* directed */
    o_directed = new types::Double(1, 1);
    *(o_directed->get()) = static_cast<double>(directed);
    graph->append(o_directed);
    /* n */
    o_n = new types::Double(1, 1);
    *(o_n->get()) = static_cast<double>(n);
    graph->append(o_n);
    /* tail */
    o_tail = new types::Double(1, ma);
    o_tail->setInt(tail);
    FREE(tail);
    graph->append(o_tail);
    /* head */
    o_head = new types::Double(1, ma);
    o_head->setInt(head);
    FREE(head);
    graph->append(o_head);
    /* node_name */
    o_node_name = new types::String(1, ndim, (wchar_t**)node_name);
    FREE(node_name);
    graph->append(o_node_name);
    /* node_type */
    o_node_type = new types::Double(1, ndim);
    o_node_type->setInt(node_type);
    FREE(node_type);
    graph->append(o_node_type);
    /* node_x */
    o_node_x = new types::Double(1, ndim);
    o_node_x->setInt(node_x);
    FREE(node_x);
    graph->append(o_node_x);
    /* node_y */
    o_node_y = new types::Double(1, ndim);
    o_node_y->setInt(node_y);
    FREE(node_y);
    graph->append(o_node_y);
    /* node_color */
    o_node_color = new types::Double(1, ndim);
    o_node_color->setInt(node_color);
    FREE(node_color);
    graph->append(o_node_color);
    /* node_diam */
    o_node_diam = new types::Double(1, ndim);
    o_node_diam->setInt(node_diam);
    FREE(node_diam);
    graph->append(o_node_diam);
    /* node_border */
    o_node_border = new types::Double(1, ndim);
    o_node_border->setInt(node_border);
    FREE(node_border);
    graph->append(o_node_border);
    /* node_font_size */
    o_node_font_size = new types::Double(1, ndim);
    o_node_font_size->setInt(node_font_size);
    FREE(node_font_size);
    graph->append(o_node_font_size);
    /* node_demand */
    o_node_ = new types::Double(1, ndim, node_demand);
    FREE(node_demand);
    graph->append(o_node_demand);
    /* edge_name */
    o_edge_name = new types::String(1, ma, (wchar_t**)edge_name);
    FREE(edge_name);
    graph->append(o_edge_name);
    /* edge_color */
    o_edge_color = new types::Double(1, ma);
    o_edge_color->setInt(edge_color);
    FREE(edge_color);
    graph->append(o_edge_color);
    /* edge_width */
    o_edge_width = new types::Double(1, ma);
    o_edge_width->setInt(edge_width);
    FREE(edge_width);
    graph->append(o_edge_width);
    /* edge_hi_width */
    o_edge_hi_width = new types::Double(1, ma);
    o_edge_hi_width->setInt(edge_hi_width);
    FREE(edge_hi_width);
    graph->append(o_edge_hi_width);
    /* edge_font_size */
    o_edge_font_size = new types::Double(1, ma);
    o_edge_font_size->setInt(edge_font_size);
    FREE(edge_font_size);
    graph->append(o_edge_font_size);
    /* edge_length */
    o_edge_length = new types::Double(1, ma, edge_length);
    FREE(edge_length);
    graph->append(o_edge_length);
    /* edge_cost */
    o_edge_cost = new types::Double(1, ma, edge_cost);
    FREE(edge_cost);
    graph->append(o_edge_cost);
    /* edge_min_cap */
    o_edge_min_cap = new types::Double(1, ma, edge_min_cap);
    FREE(edge_min_cap);
    graph->append(o_edge_min_cap);
    /* edge_max_cap */
    o_edge_max_cap = new types::Double(1, ma, edge_max_cap);
    FREE(edge_max_cap);
    graph->append(o_edge_max_cap);
    /* edge_q_weight */
    o_edge_q_weight = new types::Double(1, ma, edge_q_weight);
    FREE(edge_q_weight);
    graph->append(o_edge_q_weight);
    /* edge_q_orig */
    o_edge_q_orig = new types::Double(1, ma, edge_q_orig);
    FREE(edge_q_orig);
    graph->append(o_edge_q_orig);
    /* edge_weight */
    o_edge_weight = new types::Double(1, ma, edge_weight);
    FREE(edge_weight);
    graph->append(o_edge_weight);
    /* default_node_diam */
    o_default_node_diam = new types::Double(1, 1);
    *(o_default_node_diam) = static_cast<double>(default_node_diam);
    graph->append(o_default_node_diam);
    /* default_node_border */
    o_default_node_border = new types::Double(1, 1);
    *(o_default_node_border) = static_cast<double>(default_none_border);
    graph->append(o_default_node_border);
    /* default_edge_width */
    o_default_edge_width = new types::Double(1, 1);
    *(o_default_edge_width) = static_cast<double>(o_default_edge_width);
    graph->append(o_default_edge_width);
    /* default_edge_hi_width */
    o_default_edge_hi_width = new types::Double(1, 1);
    *(o_default_edge_hi_width)
        = static_cast<double>(o_default_edge_hi_width);
    graph->append(o_default_edge_hi_width);
    /* default_font_size */
    o_default_font_size = new types::Double(1, 1);
    *(o_default_font_size) = static_cast<double>(o_default_font_size);
    graph->append(o_default_font_size);

    /* return output variables as doubles */
    out->push_back(graph);

    return 0;

}
