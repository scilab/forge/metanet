/*
 *  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 *  Copyright (C) 2010-2010 - DIGITEO - Allan SIMON
 *
 *  This file must be used under the terms of the CeCILL.
 *  This source file is licensed as described in the file COPYING, which
 *  you should have received as part of this distribution.  The terms
 *  are also available at
 *  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "callMetanet.hxx"

extern "C"
{
#include "api_scilab.h"
// export a C symbol
int sci_close_metanet_from_scilab(char * fname, void * pvApiCtx);
}

int sci_close_metanet_from_scilab(char * fname, void * pvApiCtx)
{
    CheckRhs(0, 0);
    CheckLhs(0, 1);

    closeMetanetFromScilab();

    LhsVar(1) = 0;
    PutLhsVar();
    return 0;
}

