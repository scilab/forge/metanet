/*
 *  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 *  Copyright (C) 2010-2010 - DIGITEO - Bruno JOFRET
 *
 *  This file must be used under the terms of the CeCILL.
 *  This source file is licensed as described in the file COPYING, which
 *  you should have received as part of this distribution.  The terms
 *  are also available at
 *  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include <iostream>
#include "callMetanet.hxx"

extern "C" {
#include <string.h>
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "sci_malloc.h"
#include "getFullFilename.h"
// export a C symbol
int sci_metanet_edit_graph(char * fname, void * pvApiCtx);
}

int sci_metanet_edit_graph(char * fname, void * pvApiCtx)
{
    CheckRhs(2, 5);
    CheckLhs(0, 1);
    SciErr sciErr;

    int * piAddress;
    double * data;
    int m, n;

    char * variable  = NULL;
    char * file      = NULL;
    char * full_file = NULL;
    double window = -1.0;
    double scale = 1.0;
    double size[2] = { 0, 0 };

    int rhs_counter;
    for (rhs_counter = 1; rhs_counter <= Rhs; rhs_counter++)
    {
        sciErr = getVarAddressFromPosition(pvApiCtx, rhs_counter, &piAddress);
        if (sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        if (!isEmptyMatrix(pvApiCtx, piAddress))
        {
            switch (rhs_counter)
            {
            case 1:
                if (getAllocatedSingleString(pvApiCtx, piAddress, &variable))
                    goto return_false;
                break;
            case 2:
                if (getAllocatedSingleString(pvApiCtx, piAddress, &file))
                    goto return_false;
                full_file = getFullFilename(file);
                break;
            case 3:
                if (getScalarDouble(pvApiCtx, piAddress, &window))
                    goto return_false;
                break;
            case 4:
                if (getScalarDouble(pvApiCtx, piAddress, &scale))
                    goto return_false;
                break;
            case 5:
                if (!isDoubleType(pvApiCtx, piAddress))
                    goto return_false;
                sciErr = getMatrixOfDouble(pvApiCtx, piAddress, &m, &n, &data);
                if (sciErr.iErr)
                    goto return_false;
                if (m > 0
                 && n > 0
                 && m*n == 2)
                {
                    size[0] = data[0];
                    size[1] = data[1];
                }
                break;
            default:
                break;
            }
        }
    }

    if (callMetanet(variable, full_file, window, scale, size, 2))
        goto return_false;

return_true:
    LhsVar(1) = 1;
    PutLhsVar();
    if (variable)  FREE(variable);
    if (file)      FREE(file);
    if (full_file) FREE(full_file);
    return 0;

return_false:
    LhsVar(1) = 0;
    PutLhsVar();
    if (variable)  FREE(variable);
    if (file)      FREE(file);
    if (full_file) FREE(full_file);
    return 1;
}

