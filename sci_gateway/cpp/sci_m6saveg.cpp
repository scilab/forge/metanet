
/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "function.hxx"
#include "double.hxx"
#include "list.hxx"
#include "string.hxx"

extern "C"
{
#include "Scierror.h"
#include "localization.h"
#include "sci_malloc.h" // FREE
void C2F(saveg)(
           char * path,                  int * lpath,
          char ** name,                  int * lname,
            int * directed,              int * node_number,
           int ** tail,                 int ** head,
         char *** node_name,            int ** node_type,
           int ** node_x,               int ** node_y,
           int ** node_color,           int ** node_diam,
           int ** node_border,          int ** node_font_size,
        double ** node_demand,        char *** edge_name,
           int ** edge_color,           int ** edge_width,
           int ** edge_hi_width,        int ** edge_font_size,
        double ** edge_length,       double ** edge_cost,
        double ** edge_min_cap,      double ** edge_max_cap,
        double ** edge_q_weight,     double ** edge_q_orig,
        double ** edge_weight,           int * default_node_diam,
            int * default_node_border,   int * default_edge_width,
            int * default_edge_hi_width, int * default_font_size,
            int * ma);
}

types::Function::ReturnValue
sci_saveg(
        typed_list & in,
        int iRetCount,
        typed_list & out)
{

    types::List   * o_g       = NULL;
    types::String * o_name    = NULL;
    types::Double * o_ma      = NULL;
    types::String * o_datanet = NULL;
    int num_node = 0;

    char * fname = "saveg";

    if (in.size() != 4)
    {
        Scierror(999, _("%s: Invalid number of input arguments: "
                        "Expected %d, got %d instead.\n"),
                 fname, 4, in.size());
        return 1;
    }

    if (iRetCount != 1)
    {
        Scierror(999, _("%s: Invalid number of output arguments: "
                        "Expected %d, got %d instead.\n"),
                 fname, 1, iRetCount);
        return 1;
    }

    /* checking variable g (number 1) */
    if (in[0]->isList() == false)
    {
        Scierror(999, _("%s: Invalid type for input argument "
                        "%s at position #%d: Expected a %s "
                        "but got a %s instead.\n"),
                 fname, "g", 1, "list", in[0]->getTypeStr());
        return 1;
    }
    g_name = reinterpret_cast<char*>(
            o_g->get(0)->getAs<types::String>()->get());
    g_lname = static_cast<int>(*(
                o_g->get(1)->getAs<types::Double>()->get()));
    g_directed = static_cast<int>(*(
                o_g->get(2)->getAs<types::Double>()->get()));
    g_n = static_cast<int>(*(
                o_g->get(3)->getAs<types::Double>()->get()));
    g_tail = o_g->get(4)->getAs<types::Double>()->get();
    g_head = o_g->get(5)->getAs<types::Double>()->get();
    g_node_name = reinterpret_cast<char*>(
            o_g0>get(6)->getAs<types::String>()->get());
    g_node_type = 
    /* check list element types (could check dims too) */
    o_g = in[0]->getAs<types::List>();
    if (o_g->get(0)->isString() == false
     || o_g->get(1)->isDouble() == false
     || o_g->get(2)->isDouble() == false
     || o_g->get(3)->isDouble() == false
     || o_g->get(4)->isDouble() == false
     || o_g->get(5)->isDouble() == false
     || o_g->get(6)->isString() == false
     || o_g->get(7)->isDouble() == false
     || o_g->get(8)->isDouble() == false
     || o_g->get(9)->isDouble() == false
     || o_g->get(10)->isDouble() == false
     || o_g->get(11)->isDouble() == false
     || o_g->get(12)->isDouble() == false
     || o_g->get(13)->isDouble() == false
     || o_g->get(14)->isDouble() == false
     || o_g->get(15)->isString() == false
     || o_g->get(16)->isDouble() == false
     || o_g->get(17)->isDouble() == false
     || o_g->get(18)->isDouble() == false
     || o_g->get(19)->isDouble() == false
     || o_g->get(20)->isDouble() == false
     || o_g->get(21)->isDouble() == false
     || o_g->get(22)->isDouble() == false
     || o_g->get(23)->isDouble() == false
     || o_g->get(24)->isDouble() == false
     || o_g->get(25)->isDouble() == false
     || o_g->get(26)->isDouble() == false
     || o_g->get(27)->isDouble() == false
     || o_g->get(28)->isDouble() == false
     || o_g->get(29)->isDouble() == false
     || o_g->get(30)->isDouble() == false
     || o_g->get(31)->isDouble() == false)
    {
        Scierror(999, _("%s: Invalid sub-item type for "
                        "list %s at position #%d.\n"),
                 fname, "g", 1);
        return 1;
    }

    /* checking variable name (number 2) */
    if (in[1]->isString() == false)
    {
        Scierror(999, _("%s: Invalid type for argument "
                        "%s at position #%d: Expected a %s "
                        "but got a %s instead.\n"),
                 fname, "name", 2, "string", in[1]->getTypeStr());
        return 1;
    }
    o_name = in[1]->getAs<types::String>();
    if (o_name->getSize() != 1)
    {
        Scierror(999, _("%s: Expected a (1, 1) for "
                        "input argument %s at position #%d, "
                        "but got a (%d, %d) instead.\n"),
                 fname, "name", 2,
                 o_name->getRows(), o_name->getCols());
        return 1;
    }
    pc_name = static_cast<char*>(o_name->get());
    l_name = 0;
    while (pc_name[l_name])
        l_path++;

    /* checking variable ma (number 3) */
    if (in[2]->isDouble() == false)
    {
        Scierror(999, _("%s: Invalid type for input argument "
                        "%s at position #%d: Expected a %s "
                        "but got a %s instead.\n"),
                 fname, "ma", 3, "double", in[2]->getTypeStr());
        return 1;
    }
    o_ma = in[2]->getAs<types::Double>();
    if (o_ma->isScalar() == false)
    {
        Scierror(999, _("%s: Expected a (1, 1) for "
                        "input argument %s at position #%d, "
                        "but got a (%d, %d) instead.\n"),
                 fname, "ma", 3,
                 o_ma->getRows(), o_ma->getCols());
        return 1;
    }

    /* checking variable datanet (number 4) */
    if (in[3]->isString() == false)
    {
        Scierror(999, _("%s: Invalid type for argument "
                        "%s at position #%d: Expected a %s "
                        "but got a %s instead.\n"),
                 fname, "datanet", 4, "string", in[1]->getTypeStr());
        return 1;
    }
    o_datanet = in[3]->getAs<types::String>();
    if (o_datanet->getSize() != 1)
    {
        Scierror(999, _("%s: Expected a (1, 1) for "
                        "input argument %s at position #%d, "
                        "but got a (%d, %d) instead.\n"),
                 fname, "datanet", 4,
                 o_datanet->getRows(), o_datanet->getCols());
        return 1;
    }
    pc_datanet = static_cast<char*>(o_datanet->get());
    l_datanet = 0;
    while (pc_datanet[l_datanet])
        l_datanet++;

    /* cross variable size checking */
    tail_cols      = o_g->get(4)->getAs<types::Double>()->getCols();
    node_name_cols = o_g->get(6)->getAs<types::String>()->getCols();
    if (o_g->get(5 )->getAs<types::Double>()->getCols() != tail_cols
     || o_g->get(15)->getAs<types::String>()->getCols() != tail_cols
     || o_g->get(16)->getAs<types::Double>()->getCols() != tail_cols
     || o_g->get(17)->getAs<types::Double>()->getCols() != tail_cols
     || o_g->get(18)->getAs<types::Double>()->getCols() != tail_cols
     || o_g->get(19)->getAs<types::Double>()->getCols() != tail_cols
     || o_g->get(20)->getAs<types::Double>()->getCols() != tail_cols
     || o_g->get(21)->getAs<types::Double>()->getCols() != tail_cols
     || o_g->get(22)->getAs<types::Double>()->getCols() != tail_cols
     || o_g->get(23)->getAs<types::Double>()->getCols() != tail_cols
     || o_g->get(24)->getAs<types::Double>()->getCols() != tail_cols
     || o_g->get(25)->getAs<types::Double>()->getCols() != tail_cols
     || o_g->get(25)->getAs<types::Double>()->getCols() != tail_cols
     ||  o_g->get(6 )->getAs<types::String>()->getRows()
      != o_g->get(15)->getAs<types::Double>()->getRows()
     || o_g->get(7 )->getAs<types::Double>()->getCols() != node_name_cols
     || o_g->get(8 )->getAs<types::Double>()->getCols() != node_name_cols
     || o_g->get(9 )->getAs<types::Double>()->getCols() != node_name_cols
     || o_g->get(10)->getAs<types::Double>()->getCols() != node_name_cols
     || o_g->get(11)->getAs<types::Double>()->getCols() != node_name_cols
     || o_g->get(12)->getAs<types::Double>()->getCols() != node_name_cols
     || o_g->get(13)->getAs<types::Double>()->getCols() != node_name_cols
     || o_g->get(14)->getAs<types::Double>()->getCols() != node_name_cols)
    {
        Scierror(999, _("%s: Error on cross variable size check.\n"),
                 fname);
        return 1;
    }
    /***********************/
    /*                     */
    /*    CALL TO SAVEG    */
    /*                     */
    /***********************/
    C2F(loadg)(
            /* input */
            path,                   &lpath,
            /* output */
            &name,                  &lname,
            &directed,              &node_number,
            &tail,                  &head,
            &node_name,             &node_type,
            &node_x,                &node_y,
            &node_color,            &node_diam,
            &node_border,           &node_font_size,
            &node_demand,           &edge_name,
            &edge_color,            &edge_width,
            &edge_hi_width,         &edge_font_size,
            &edge_length,           &edge_cost,
            &edge_min_cap,          &edge_max_cap,
            &edge_q_weight,         &edge_q_orig,
            &edge_weight,           &default_node_diam,
            &default_node_border,   &default_edge_width,
            &default_edge_hi_width, &default_font_size,
            &(static_cast<int>(ma->get())));

}
