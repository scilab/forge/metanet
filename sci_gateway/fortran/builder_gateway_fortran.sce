// ====================================================================
// Allan CORNET - DIGITEO - 2012 - Allan CORNET
// Simon LIPP
// INRIA 2008
// This file is released into the public domain
// ====================================================================

function builder_gw_fortran()

    fortran_functions = [
        "m6loadg",    "intsm6loadg_",
        "m6saveg",    "intsm6saveg_",
        "m6prevn2p",  "intsm6prevn2p_",
        "m6ns2p",     "intsm6ns2p_",
        "m6p2ns",     "intsm6p2ns_",
        "m6edge2st",  "intsm6edge2st_",
        "m6prevn2st", "intsm6prevn2st_",
        "m6compc",    "intsm6compc_",
        "m6concom",   "intsm6concom_",
        "m6compfc",   "intsm6compfc_",
        "m6sconcom",  "intsm6sconcom_",
        "m6pcchna",   "intsm6pcchna_",
        "m6fordfulk", "intsm6fordfulk_",
        "m6johns",    "intsm6johns_",
        "m6dijkst",   "intsm6dijkst_",
        "m6frang",    "intsm6frang_",
        "m6chcm",     "intsm6chcm_",
        "m6transc",   "intsm6transc_",
        "m6dfs",      "intsm6dfs_",
        "m6umtree",   "intsm6umtree_",
        "m6umtree1",  "intsm6umtree1_",
        "m6dmtree",   "intsm6dmtree_",
        "m6tconex",   "intsm6tconex_",
        "m6flomax",   "intsm6flomax_",
        "m6kilter",   "intsm6kilter_",
        "m6busack",   "intsm6busack_",
        "m6floqua",   "intsm6floqua_",
        "m6relax",    "intsm6relax_",
        "m6findiso",  "intsm6findiso_",
        "m6ta2lpu",   "intsm6ta2lpu_",
        "m6lp2tad",   "intsm6lp2tad_",
        "m6lp2tau",   "intsm6lp2tau_",
        "m6dfs2",     "intsm6dfs2_",
        "m6diam",     "intsm6diam_",
        "m6cent",     "intsm6cent_",
        "m6hullcvex", "intsm6hullcvex_",
        "m6clique",   "intsm6clique_",
        "m6clique1",  "intsm6clique1_",
        "m6hamil",    "intsm6hamil_",
        "m6visitor",  "intsm6visitor_",
        "m6bmatch",   "intsm6bmatch_",
        "m6knapsk",   "intsm6knapsk_",
        "m6prfmatch", "intsm6prfmatch_",
        "m6permuto",  "intsm6permuto_",
        "m6mesh2b",   "intsm6mesh2b_",
        "m6deumesh",  "intsm6deumesh_",
        "m6bandred",  "intsm6bandred_",
        "m6meshmesh", "intsm6meshmesh_",
        "m6ford",     "intsm6ford_"];

  fortran_files = [
    "sci_m6visitor.f",
    "sci_m6umtree1.f",
    "sci_m6umtree.f",
    "sci_m6transc.f",
    "sci_m6tconex.f",
    "sci_m6ta2lpu.f",
    "sci_m6sconcom.f",
    "sci_m6saveg.f",
    "sci_m6relax.f",
    "sci_m6prfmatch.f",
    "sci_m6prevn2st.f",
    "sci_m6prevn2p.f",
    "sci_m6permuto.f",
    "sci_m6pcchna.f",
    "sci_m6p2ns.f",
    "sci_m6ns2p.f",
    "sci_m6meshmesh.f",
    "sci_m6mesh2b.f",
    "sci_m6lp2tau.f",
    "sci_m6lp2tad.f",
    "sci_m6loadg.f",
    "sci_m6knapsk.f",
    "sci_m6kilter.f",
    "sci_m6johns.f",
    "sci_m6hullcvex.f",
    "sci_m6hamil.f",
    "sci_m6frang.f",
    "sci_m6fordfulk.f",
    "sci_m6ford.f",
    "sci_m6floqua.f",
    "sci_m6flomax.f",
    "sci_m6findiso.f",
    "sci_m6edge2st.f",
    "sci_m6dmtree.f",
    "sci_m6dijkst.f",
    "sci_m6diam.f",
    "sci_m6dfs2.f",
    "sci_m6dfs.f",
    "sci_m6deumesh.f",
    "sci_m6concom.f",
    "sci_m6compfc.f",
    "sci_m6compc.f",
    "sci_m6clique1.f",
    "sci_m6clique.f",
    "sci_m6chcm.f",
    "sci_m6cent.f",
    "sci_m6busack.f",
    "sci_m6bmatch.f",
    "sci_m6bandred.f"];

    WITHOUT_AUTO_PUTLHSVAR = %t;
    LIBS = [
        '../../src/fortran/libmetanet_f',
        '../../src/c/libmetanet_c',
        '../../src/cpp/libmetanet_cpp'];

    LDFLAGS = '';

    if getos() == 'Windows' then
        LDFLAGS = SCI + '/bin/elementary_functions.lib ' ..
                + SCI + '/bin/elementary_functions_f.lib';
    else
        LDFLAGS = "-lgfortran";
    end


  tbx_build_gateway('gw_metanet_fortran', fortran_functions, fortran_files, ..
                  get_absolute_file_path('builder_gateway_fortran.sce'),  ..
                  LIBS, LDFLAGS);
endfunction

builder_gw_fortran();
clear builder_gw_fortran;

