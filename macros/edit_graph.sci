//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - Scilab Enterprises - Clement David
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function edit_graph(graph, scale, winsize)
    if exists("graph", "local") == 0 then
        metanet_edit_graph([], []);
        return;
    end

    if exists('scale','local') == 0 then
        scale = 1.0;
    end
    if exists('winsize','local') == 0 then
        winsize = [600 400];
    end

    // normal case, call edit_graph with a path
    // to an xgraph file or to a graph structure
    select typeof(graph)
    case "string" then
        len = length(graph);
        ext = part(graph, (len-5):len);
        if ext == "xgraph" then
            metanet_edit_graph([], graph, -1, scale, winsize);
            return;
        end
    case "graph" then
        g = graph;
        metanet_edit_graph("g", [], -1, scale, winsize);
        return;
    end

    // compatibility case, load_graph first then open with variable name
    g = load_graph(graph);
    metanet_edit_graph("g", [], -1, scale, winsize);
endfunction
