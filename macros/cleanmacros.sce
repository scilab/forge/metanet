// ====================================================================
// Allan CORNET
// DIGITEO 2009
// This file is released into the public domain
// ====================================================================
libpath = get_absolute_file_path('cleanmacros.sce');

libpath_editor = libpath+'/editor';
libpath_graph_tools = libpath+'/graph_tools';
libpath = [libpath_editor;libpath_graph_tools];

binfiles = ls(libpath+'/*.bin');
for i = 1:size(binfiles,'*')
  mdelete(binfiles(i));
end

mdelete(libpath_editor+'/names');
mdelete(libpath_graph_tools+'/names');

mdelete(libpath_editor+'/lib');
mdelete(libpath_graph_tools+'/lib');

clear libpath;
// ====================================================================
