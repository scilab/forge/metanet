// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO Allan SIMON
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// return the jgraphx ids of a vector of nodes index
// indexesVector => vector of integer, the index of Nodes in the graph structure
// graphStructure the metanet structure to look in


function idsVector=nodes_2_ids(indexesVector,graphStructure)
  [lhs,rhs]=argn(0)
  if rhs<>2 then error(39), end
  
  //check indexesVector
  s = size(indexesVector);
  if s(1)*s(2) == 0 then idsVector=[]; return end
  if s(1)<>1 then
    error('First argument must be a row vector');
  end
  for i=1:size(indexesVector,'*')
    currentIndex = indexesVector(i)
    idsVector(1,i) = graphStructure.nodes.graphics.id(currentIndex)
  end

endfunction

