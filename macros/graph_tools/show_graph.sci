
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2002-2008 - INRIA - Serge STEER <serge.steer@inria.fr>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function win = show_graph(GraphList,smode,scale,winsize)
  global EGcurrent
  select argn(2)
  case 1 then
    smode='new',scale=1,winsize=[600 400]
  case 2 then
    if type(smode)==10 then
      scale=1,
    else
      scale=smode
      smode='rep'
    end
    winsize=[600 400]
  case 3 then
    winsize=[600 400]
  end
  if and(smode<>['rep','new']) then 
    error('Second argument should be ""rep"" or ""new""')
  end

  if smode=='new' then
    g=GraphList;
    metanet_edit_graph('g', [], -1, scale, winsize);
  else //replace
    win = netwindows();
    metanet_edit_graph('g', [], win(2), scale, winsize);
  end
  
  win = netwindows();
  win = win(2);
endfunction
