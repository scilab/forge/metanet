// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO Allan SIMON
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// return the jgraphx indexes of a vector of nodes ids
// indexesVector => vector of integer, the index of Nodes in the graph structure
// graphStructure the metanet structure to look in


function indexesVector=nodes_ids_2_indexes(idsVector,graphStructure)
  [lhs,rhs]=argn(0)
  if rhs<>2 then error(39), end
  
  //check idsVector
  s = size(idsVector);
  if s(1)*s(2) == 0 then indexesVector=[]; return end
  if s(1)<>1 then
    error('First argument must be a row vector');
  end
  indexesVector = []
  for i=1:size(idsVector,'*')
    currentId = idsVector(i)
    for j=1:size(graphStructure.nodes.graphics.id,"c")
      if currentId == graphStructure.nodes.graphics.id(j) then
        indexesVector(1,i) = j
        break
      end
    end
  end

endfunction

