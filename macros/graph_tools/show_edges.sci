
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2002-2008 - INRIA - Serge STEER <serge.steer@inria.fr>
// Copyright (C) 2012-2015 - Scilab Enterprises - Clement David
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function show_edges(p,sup,leg)
  if exists('leg','local')==0 then leg='nothing',end
  if exists('sup','local')==0 then sup='no',end
  if type(p)<>1 then error(52,1),end

  //get current editgraph window
  wins = netwindows();
  if wins(2)==0 then
    error(msprintf('%s: no current edit_graph window defined, use netwindow', "show_edges"));
  end
  
  metanet_show(%f, %t, p, sup=='sup', leg);
endfunction
