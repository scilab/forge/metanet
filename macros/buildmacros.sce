// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (c) 2009-2012 - DIGITEO - Allan CORNET 
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function build_macros()
  curpathmacros = get_absolute_file_path("buildmacros.sce")
  tbx_build_macros(TOOLBOX_NAME, get_absolute_file_path("buildmacros.sce"));
  exec(curpathmacros + '/editor/buildmacros.sce');
  exec(curpathmacros + '/graph_tools/buildmacros.sce');
endfunction

build_macros();
clear build_macros;

