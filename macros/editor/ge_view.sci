
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2002-2008 - INRIA - Serge STEER <serge.steer@inria.fr>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function ge_view(kmen,win)
  w=string(win)
  execstr('global EGdata_'+w+'; mens=EGdata_'+w+'.Menus(3)(2:$)')
  // just call ge_eventhandler to have all event treated in the same function
  ge_eventhandler(win,-1,-1,mens(kmen))
endfunction
