
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2002-2008 - INRIA - Serge STEER <serge.steer@inria.fr>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function ge_drawarcs(sel)
    arrowWidth = 6;
    arrowLength = 6;
    if or(type(sel) == [2 129]) then
        sel = horner(sel, size(GraphList.tail, "*"));
    elseif size(sel, 1) == -1 then
        sel = 1:size(GraphList.tail, "*");
    end
    if GraphList.tail(sel) == [] then
        return;
    end

    [xx, yy, sel1, loops] = ge_arc_coordinates(sel);

    ge_draw_std_arcs(xx, yy, ge_get_arcs_id(sel1));

    if loops <> [] then
        loops = sel(loops);
        ge_draw_loop_arcs(loops);
    end

    if xget("pixmap") then
        xset("wshow");
    end
endfunction

