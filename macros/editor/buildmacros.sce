
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (c) 2008 - INRIA - Pierre MARECHAL <pierre.marechal@inria.fr>
// Copyright (C) 2009-2012 - DIGITEO - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function build_macros_editor()
    curpath = get_absolute_file_path("buildmacros.sce");
    genlib("metaneteditorlib", curpath, %f, %t);
endfunction

build_macros_editor();
clear build_macros_editor;

