<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2008 - INRIA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en" xml:id="circuit">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>
  <refnamediv>
    <refname>circuit</refname>
    <refpurpose> finds a circuit or the rank function in a directed graph</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>[p,r] = circuit(g)</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>g</term>
        <listitem>
          <para> a <link linkend="graph_data_structure">graph_data_structure</link>.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>p</term>
        <listitem>
          <para>row vector of integer numbers of the arcs of the circuit if it exists</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>r</term>
        <listitem>
          <para>row vector of rank function if there is no circuit</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>A cycle of a graph <literal>g</literal>, also called a
    circuit, is a subset of the edges  of <literal>g</literal> that forms
    a path such that the first node of the path corresponds to the
    last.</para>
    <para><literal>circuit</literal> tries to find surch a circuit
    for the directed graph <literal>g</literal>.  It returns the circuit
    <literal>p</literal> as a row vector of the corresponding arc numbers if
    it exists and it returns the empty vector <literal>[]</literal>
    otherwise.</para>
    <para>If the graph has no circuit, the rank function is returned in <literal>r</literal>, 
    otherwise its value is the empty vector <literal>[]</literal>.</para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[ 
// graph with circuit
ta=[1 1 2 3 5 4 6 7 7 3 3 8 8 5];
he=[2 3 5 4 6 6 7 4 3 2 8 1 7 4];
g=make_graph('foo',1,8,ta,he);
g.nodes.graphics.x=[116 231 192 323 354 454 305 155];
g.nodes.graphics.y=[ 118 116 212 219 117 185 334 316];
g.nodes.graphics.display='number';
g.edges.graphics.display='number';

show_graph(g);
p=circuit(g)
hilite_edges(p)

// graph without circuit
g1=make_graph('foo',1,4,[1 2 2 3],[2 3 4 4]);
g1.nodes.graphics.x=[116 231 192 323];
g1.nodes.graphics.y=[ 118 116 212 219];
g1.nodes.graphics.display='number';
g1.edges.graphics.display='number';

show_graph(g1,'new');
[p,r]=circuit(g)
 ]]></programlisting>
  </refsection>
  <refsection>
    <title>Bibliography</title>
    <para>Stanley, R. P. Enumerative Combinatorics, Vol. 1. Cambridge, England: Cambridge University Press, 1999.</para>
  </refsection>
</refentry>
