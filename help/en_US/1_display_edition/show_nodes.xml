<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2008 - INRIA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en" xml:id="show_nodes">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>
  <refnamediv>
    <refname>show_nodes</refname>
    <refpurpose> highlights a set of nodes</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>show_nodes(nodes [,sup])
show_nodes(nodes ,sup=value)
show_nodes(nodes ,leg=value)
show_nodes(nodes ,sup=value,leg=value)</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>nodes</term>
        <listitem>
          <para>row vector of node numbers</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>sup</term>
        <listitem>
          <para>string, superposition flag. The default value is  <literal>'no'</literal>.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>leg</term>
        <listitem>
          <para>string, data field to be displayed. The default value is <literal>'nothing'</literal>.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para><literal>show_nodes</literal> highlights the set of nodes
    <literal>nodes</literal> of the displayed graph in the current
    <literal>edit_graph</literal> window (see <link linkend="netwindow">netwindow</link>).  If
    the optional argument <literal>sup</literal> is equal to the string
    'sup', the highlighting is superposed on the previous one.</para>
    <para>If leg is equal to <literal>'number'</literal> the node numbers are also drawn.</para>
    <para>If leg is equal to <literal>'name'</literal> the node names are  also drawn.</para>
    <para>If leg is equal to one of the node data fields the corresponding values are also drawn.</para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[ 
ta=[1 1 2 2 2 3 4 5 5 7 8 8 9 10 10 10 11 12 13 13 13 14 15 16 16 17 17];
he=[2 10 3 5 7 4 2 4 6 8 6 9 7 7 11 15 12 13 9 10 14 11 16 1 17 14 15];
g=make_graph('foo',1,17,ta,he);
g.nodes.graphics.x=[283 163 63 57 164 164 273 271 339 384 504 513 439 623 631 757 642]/2;
g.nodes.graphics.y=[59 133 223 318 227 319 221 324 432 141 209 319 428 443 187 151 301]/2;
show_graph(g);
for i=2:3:g.nodes.number, show_nodes(i); end;
for i=1:3:g.nodes.number, show_nodes(i,'sup'); end;
show_nodes(1:3:g.nodes.number,leg='number')
 ]]></programlisting>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="edit_graph">edit_graph</link>
      </member>
      <member>
        <link linkend="hilite_nodes">hilite_nodes</link>
      </member>
      <member>
        <link linkend="unhilite_nodes">unhilite_nodes</link>
      </member>
      <member>
        <link linkend="show_arcs">show_arcs</link>
      </member>
      <member>
        <link linkend="netwindow">netwindow</link>
      </member>
      <member>
        <link linkend="netwindows">netwindows</link>
      </member>
    </simplelist>
  </refsection>
</refentry>
