<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2008 - INRIA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en" xml:id="edgedatafields">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>
  <refnamediv>
    <refname>edgedatafields</refname>
    <refpurpose> returns the vector of edge data fields names</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>F = edgedatafields(g)</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>g</term>
        <listitem>
          <para>a graph data structure (see <link linkend="graph_data_structure">graph_data_structure</link> )
       </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>F</term>
        <listitem>
          <para>a row vector of strings. Each element is a field name of the edges data data structure.
       </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>It is possible to associate data to the edges of a graph. This
  can be done with the <link linkend="add_edge_data">add_edge_data</link> function. the
  <literal>edgedatafields</literal> function allows to retreive the field
  names of these data. A given  edge data can be referenced by its field name
  <literal>g.edges.data(field_name)</literal>.</para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[ 
//create a simple graph
ta=[1  1 2 7 8 9 10 10 10 10 11 12 13 13];
he=[2 10 7 8 9 7  7 11 13 13 12 13  9 10];
g=make_graph('simple',1,13,ta,he);
g.nodes.graphics.x=[40,33,29,63,146,233,75,42,114,156,237,260,159];
g.nodes.graphics.y=[7,61,103,142,145,143,43,120,145,18,36,107,107];
show_graph(g,'new')

g=add_edge_data(g,'length',round(10*rand(1,14,'u')));
g=add_edge_data(g,'label','e'+string(1:14));
edgedatafields(g)
g.edges.data.label
 ]]></programlisting>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="graph_data_structure">graph_data_structure</link>
      </member>
      <member>
        <link linkend="add_edge_data">add_edge_data</link>
      </member>
    </simplelist>
  </refsection>
</refentry>
