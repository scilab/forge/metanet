<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2008 - INRIA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en" xml:id="min_qcost_flow">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>
  <refnamediv>
    <refname>min_qcost_flow</refname>
    <refpurpose> minimum quadratic cost flow</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>[c,phi,flag] = min_qcost_flow(eps,g)</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>eps</term>
        <listitem>
          <para>scalar, precision</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>g</term>
        <listitem>
          <para> a <link linkend="graph_data_structure">graph_data_structure</link>.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>c</term>
        <listitem>
          <para>value of cost</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>phi</term>
        <listitem>
          <para>row vector of the value of flow on the arcs</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>flag</term>
        <listitem>
          <para>feasible problem flag (0 or 1)</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      <literal>min_qcost_flow</literal> computes the minimum quadratic
      cost flow in the network <literal>g</literal>. It returns the
      total cost of the flows on the arcs <literal>c</literal> and the
      row vector of the flows on the arcs
      <literal>phi</literal>. <literal>eps</literal> is the precision
      of the iterative algorithm. If the problem is not feasible
      (impossible to find a compatible flow for instance),
      <literal>flag</literal> is equal to 0, otherwise it is equal to
      1.
    </para>
 <para>
      The bounds of the flow are given by the
      <literal>g.edges.data.min_cap</literal> and
      <literal>g.edges.data.max_cap</literal> fields of the graph.
    </para>
    <para>
      The value of the minimum capacity and of the maximum capacity
      must be non negative.  The value of the maximum capacity must be
      greater than or equal to the value of the minimum capacity.
    </para>
    <para> 
      If the value of <literal>min_cap</literal> or
      <literal>max_cap</literal> is not given it is assumed to be
      equal to 0 on each edge.</para>
    <para>
      The costs on the edges are given by the elements
      <literal>g.edges.data.q_orig</literal> and
      <literal>g.edges.data.q_weight</literal>of the fields of the
      graph. These values must be non negative. If the value of
      <literal>q_orig</literal> or <literal>q_weight</literal> is not
      given, it is assumed to be equal to 0 on each edge.  The cost on
      arc <literal>u</literal> is given by:
      <literal>(1/2)*q_weight[u](phi[u]-q_orig[u])^2</literal>
    </para>
    <para>
      If the <literal>min_cap</literal> or <literal>max_cap</literal>
      or <literal>q_orig</literal> or <literal>q_weight</literal> data
      fields are not present in the graph structure they can be added
      and set using the <link
      linkend="add_edge_data">add_edge_data</link> function.
    </para>
  
   
    <para>
      This function uses an algorithm due to M. Minoux.
    </para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[ 
ta=[1 1 2 2 2 3 4 4 5 6 6 6 7 7 7 8 9 10 12 12 13 13 13 14 15 14 9 11 10 1 8];
he=[2 6 3 4 5 1 3 5 1 7 10 11 5 8 9 5 8 11 10 11 9 11 15 13 14 4 6 9 1 12 14];
g=make_graph('foo',1,15,ta,he);
g.nodes.graphics.x=[155,153,85,155,237,244,244,334,338,346,442,440,439,333,438];
g.nodes.graphics.y=[45,177,253,254,253,114,171,257,174,101,172,64,264,350,351];
show_graph(g);

ma=edge_number(g)
g=add_edge_data(g,'min_cap',[0,1,5,5,0,3,3,5,4,1,4,3,3,1,3,1,1,4,1,3,1,4,5,4,4,2,1,4,2,3,2]);
g=add_edge_data(g,'max_cap',[38,37,42,41,34,49,35,36,43,43,43,48,37,..
                                 36,42,48,44,36,30,31,30,41,32,42,34,48,32,36,36,36,30]);
g=add_edge_data(g,'q_weight',ones(1,ma));
[c,phi,flag]=min_qcost_flow(0.001,g);flag

g.edges.graphics.foreground(find(phi<>0))=color('red');
g=add_edge_data(g,'flow',phi)
g.edges.graphics.display='flow';
show_graph(g);
 ]]></programlisting>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="min_lcost_cflow">min_lcost_cflow</link>
      </member>
      <member>
        <link linkend="min_lcost_flow1">min_lcost_flow1</link>
      </member>
      <member>
        <link linkend="min_lcost_flow2">min_lcost_flow2</link>
      </member>
      <member>
        <link linkend="edges_data_structure">edges_data_structure</link>
      </member>
      <member>
        <link linkend="add_edge_data">add_edge_data</link>
      </member>
      <member>
        <link linkend="nodes_data_structure">nodes_data_structure</link>
      </member>
      <member>
        <link linkend="add_node_data">add_node_data</link>
      </member>
    </simplelist>
  </refsection>
</refentry>
