// Copyright INRIA 2008
// Copyright DIGITEO - 2012 - Allan CORNET

tbx_builder_help_lang(["en_US", "fr_FR"], ..
                      get_absolute_file_path("builder_help.sce"));
