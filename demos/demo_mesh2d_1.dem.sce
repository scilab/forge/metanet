// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//
// This file is released under the 3-clause BSD license. See COPYING-BSD.

function demo_mesh2d_1()
    // Built from mesh2d() examples: Case #2

    // DEMO START
    // scf(100001);
    // clf()

    theta = 0.025 * [1:40] * 2.*%pi;
    x = 1.0 + cos(theta);
    y = 1.0 + sin(theta);
    theta = 0.05 * [1:20] * 2.*%pi;
    x1 = 1.3 + (0.4 * cos(theta));
    y1 = 1.0 + (0.4 * sin(theta));
    theta = 0.1 * [1:10] * 2.*%pi;
    x2 = 0.5 + (0.2 * cos(theta));
    y2 = 1.0 + (0.2 * sin(theta));
    x = [x x1 x2];
    y = [y y1 y2];

    x3 = 2. * rand(1:200);
    y3 = 2. * rand(1:200);
    wai = ((x3-1) .* (x3-1) ..
         + (y3-1) .* (y3-1));
    ii = find(wai > = .94);
    x3(ii) = [];
    y3(ii) = [];
    wai = ((x3-0.5) .* (x3-0.5) ..
         + (y3-1.0) .* (y3-1.0));
    ii = find(wai < = 0.055);
    x3(ii) = [];
    y3(ii) = [];
    wai = ((x3-1.3) .* (x3-1.3) ..
         + (y3-1.0) .* (y3-1.0));
    ii = find(wai < = 0.21);
    x3(ii) = [];y3(ii) = [];
    xnew = [x x3];
    ynew = [y y3];
    fr1 = [[1:40] 1];
    fr2 = [[41:60] 41];
    fr2 = fr2($:-1:1);
    fr3 = [[61:70] 61];
    fr3 = fr3($:-1:1);
    front = [fr1 fr2 fr3];

    nu = mesh2d(xnew, ynew, front);
    nbt = size(nu, 2);
    jj = [nu(1,:) " nu(2,:)"
          nu(2,:) " nu(3,:)"
          nu(3,:) " nu(1,:)"];
    as = sparse(jj, ones(size(jj, 1), 1));
    ast = tril(as + abs(as'-as));
    [jj, v, mn] = spget(ast);
    n = size(xnew, 2);
    g = make_graph("foo", 0, n, jj(:, 1)', jj(:, 2)');
    g.nodes.graphics.x = 300 * xnew;
    g.nodes.graphics.y = 300 * ynew;
    g.nodes.graphics.defaults.diam = 10;
    show_graph(g, 0.5)

    //demo_viewCode("demo_mesh2d_1.dem.sce");
    // not set in the special show_grap window

    // --------------------------------------------------------------------------
    // DEMO END

endfunction

demo_mesh2d_1();
clear demo_mesh2d_1;

